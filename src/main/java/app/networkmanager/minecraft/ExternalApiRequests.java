package app.networkmanager.minecraft;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ExternalApiRequests extends HTTP {
    public static boolean updateVersion = false;
    private static ExternalApiRequests instance;

    private ExternalApiRequests() {
        endpoint = NetworkManager.config.external_api.endpoint;
    }

    public static ExternalApiRequests GetInstance() {
        if (instance != null)
        {
            return instance;
        }
        return new ExternalApiRequests();
    }

    public static void Reload() {
        GetInstance().endpoint = NetworkManager.config.external_api.endpoint;
    }

    @Override
    protected Map<String, String> Headers() {
        Map<String, String> map = new HashMap<>();
        map.put("X-Instance", NetworkManager.settings.get("instance_id"));
        map.put("Accept", "application/json");
        return map;
    }

    public void Stat(String stat, String value) {
        if (!NetworkManager.config.external_api.statistics) {
            return;
        }
        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                Map<String, String> data = new HashMap<>();
                data.put("game", "minecraft");
                data.put("value", value);
                Put("/core/stat/" + stat, data);
            }
        });
    }

    /*private String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }*/

    public void ServerStat() {
        if (!NetworkManager.config.external_api.statistics) {
            return;
        }
        /*NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                Map<String, String> data = new HashMap<>();
                data.put("port", "" + NetworkManager.server.port);
                data.put("game", "minecraft");
                data.put("gamemode", "java");
                data.put("name", NetworkManager.config.api.server_name);
                data.put("version", NetworkManager.instance.getClass().getPackage().getImplementationVersion());
                Post("/serverstat", data);
            }
        });*/
    }

    public void VersionCheck() {
        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                try {
                    JsonObject json = new JsonParser().parse(Get("/core/version/minecraft").content).getAsJsonObject();
                    String str_site_version = json.get("version").getAsString();
                    String[] strs_site_version = json.get("version").getAsString().split("\\.");
                    if (strs_site_version.length < 3) {
                        NetworkManager.LogError("Got malformed version from server!");
                        return;
                    }
                    int[] site_version = {Integer.parseInt(strs_site_version[0]), Integer.parseInt(strs_site_version[1]), Integer.parseInt(strs_site_version[2])};
                    String str_local_version = NetworkManager.instance.getClass().getPackage().getImplementationVersion();
                    String[] strs_local_version = NetworkManager.instance.getClass().getPackage().getImplementationVersion().split("\\.");
                    int[] local_version = {Integer.parseInt(strs_local_version[0]), Integer.parseInt(strs_local_version[1]), Integer.parseInt(strs_local_version[2])};
                    NetworkManager.LogDebug("Local: " + str_local_version + " vs. Site: " + str_site_version);
                    if (!str_site_version.equals(str_local_version)) {
                        updateVersion = true;
                        if (site_version[0] < local_version[0]) {
                            updateVersion = false;
                        }
                        if (site_version[1] == local_version[1] && site_version[2] < local_version[2]) {
                            updateVersion = false;
                        }
                        if (site_version[0] == local_version[0] && site_version[1] == local_version[1] && site_version[2] < local_version[2]) {
                            updateVersion = false;
                        }
                        if (updateVersion) {
                            NetworkManager.LogInfo("Version mismatch! New version available from your web panel!");
                            NetworkManager.getNMPlugin().scheduleRepeatingAsyncTask(new Runnable() {
                                @Override
                                public void run() {
                                    NetworkManager.LogInfo("Version mismatch! New version available from your web panel!");
                                }
                            }, 150, 300);
                        } else {
                            NetworkManager.LogInfo("Current version is newer then the API's! Check your API version.");
                        }
                    } else {
                        NetworkManager.LogInfo("You have the current version of NetworkManager from the API.");
                    }
                } catch (Exception e) {
                    NetworkManager.sentry.sendException(e);
                    NetworkManager.LogError("Failed to get version information from external server");
                    //e.printStackTrace();
                }
            }
        });
    }
}
