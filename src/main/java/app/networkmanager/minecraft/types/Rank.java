package app.networkmanager.minecraft.types;

import app.networkmanager.minecraft.NetworkManager;

public class Rank extends RealmObject implements Permissions {
    private String rank;
    private String displayName;
    private Rank inherits;
    private int subgrouping;
    private String realm;

    public Rank(String rank, String displayName, int subgrouping, String realm) {
        this.rank = rank;
        this.displayName = displayName;
        this.subgrouping = subgrouping;
        this.realm = realm;
    }

    // Run after all ranks have been created
    public void setInherits(String inherits) {
        this.inherits = NetworkManager.ranks.get(inherits);
    }

    public String getRank() {
        return rank;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Rank getInherits() {
        return inherits;
    }

    public int getSubgrouping() {
        return subgrouping;
    }

    public String getRealm() {
        return realm;
    }

    public String toString() {
        if (inherits != null)
            return "Rank[" + displayName + "|" + inherits.displayName + "] : " + realm;
        else
            return "Rank[" + displayName + "] : " + realm;
    }

    // Permissions interface
    public void insertPermission(String fullPerm, String fullRealm) {

    }

    public void deletePermission(String fullPerm, String fullRealm) {

    }

    @Override
    public Boolean hasPermission(String fullPerm, String fullRealm) {
        Boolean result = Permissions.super.hasPermission(fullPerm, fullRealm);
        if (result == null && inherits != null) {
            result = inherits.hasPermission(fullPerm, fullRealm);
        }
        return result;
    }
}
