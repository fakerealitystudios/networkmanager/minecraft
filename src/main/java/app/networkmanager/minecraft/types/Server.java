package app.networkmanager.minecraft.types;

import app.networkmanager.minecraft.ExternalApiRequests;
import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.Realms;
import app.networkmanager.minecraft.events.server.ServerIndexed;
import app.networkmanager.minecraft.events.server.ServerSessionIndexed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Server {
    public String ip;
    public int port;
    public boolean indexed = false;
    public int sessionid = -1;
    public List<Ban> bans = new ArrayList<>();

    public Server() {
        ip = NetworkManager.getNMPlugin().getIp();
        try {
            String jsonstr = ExternalApiRequests.GetInstance().Get("/core/ip").content;
            JsonObject json = new JsonParser().parse(jsonstr).getAsJsonObject();
            if (json.has("ip")) {
                ip = json.get("ip").getAsString();
            }
        } catch(Exception e) {
            NetworkManager.sentry.sendException(e);
            // It's not the end of the world if we fail to get our IP
        }
        port = NetworkManager.getNMPlugin().getPort();
        register();
        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                collectBans();
            }
        });

        ExternalApiRequests.GetInstance().ServerStat();
    }

    private void startSession() {
        JsonObject json = NetworkManager.api.Put("/server/" + NetworkManager.GetSID() + "/session").json;
        if (json.get("status").getAsString().equals("success")) {
            sessionid = json.get("ssid").getAsInt();
            NetworkManager.callEvent(new ServerSessionIndexed(sessionid));
        }
    }

    public void register() {
        Map<String, String> formData = new HashMap<>();
        formData.put("address", ip + ":" + port);
        formData.put("name", NetworkManager.config.api.server_name);
        formData.put("game", "minecraft");
        formData.put("gamemode", "java");
        JsonObject json = NetworkManager.api.Put("/server/" + NetworkManager.GetSID(), formData).json;
        if (json.get("status").getAsString().equals("failure")) {
            NetworkManager.LogError("Couldn't connect to NetworkManager API!");
            return;
        }
        JsonObject srv = json.get("server").getAsJsonObject();
        if (srv.has("id")) {
            NetworkManager.config_server.server_id = srv.get("id").getAsInt();
            Realms.realm = NetworkManager.config_server.server_id + ".minecraft";
        }
        NetworkManager.config_server.secret = srv.get("token").getAsString();
        for (int i = 0; i < srv.get("settings").getAsJsonArray().size(); i++) {
            JsonObject setting = srv.get("settings").getAsJsonArray().get(i).getAsJsonObject();
            NetworkManager.settings.put(setting.get("name").getAsString(), setting.get("value").getAsString());
        }
        NetworkManager.config_server.settings = srv.get("settings").getAsJsonArray().toString();
        NetworkManager.config_server.writeYaml();
        //NetworkManager.settings
        NetworkManager.callEvent(new ServerIndexed(NetworkManager.config_server.server_id));
        startSession();
        pollActions();
    }

    private void pollActions() {
        NetworkManager.getNMPlugin().scheduleRepeatingAsyncTask(new Runnable() {
            @Override
            public void run() {
                JsonObject json = NetworkManager.api.Get("/server/" + NetworkManager.GetSID() + "/actions").json;
                if (json.has("actions")) {
                    for (int i = 0, size = json.get("actions").getAsJsonArray().size(); i < size; i++) {
                        
                        String payload = json.get("actions").getAsJsonArray().get(i).getAsString();
                        processPayload(new JsonParser().parse(payload).getAsJsonObject());
                    }
                }
            }
        }, 0, NetworkManager.config.api.server_action_poll); // idk, it's seconds * 20 for the ticks
    }

    // The payload should've already been validated before this point
    // and the skey removed if it's provided
    private void processPayload(JsonObject payload) {
        try {
            String action = payload.get("action").getAsString();
            NetworkManager.LogInfo("Processing payload action: " + action);
            if (action.equals("command")) {
                JsonArray jarray = payload.get("cmd").getAsJsonArray();
                List<String> list = new ArrayList<>();
                for(int i = 0; i < jarray.size(); i++) {
                    list.add(jarray.get(i).getAsString());
                }
                // We'll perform the command on the main thread
                NetworkManager.getNMPlugin().runSyncTask(() -> {
                    NetworkManager.getNMPlugin().dispatchCommand(String.join(" ", list));
                });
            } else if (action.equals("merge")) {
                UUID uuid = Player.getUUID(payload.get("nmid").getAsInt());
                if (uuid == null) { return; }
                // We're being told to perform an account merger!
                NetworkManager.reloadPlayer(uuid);
            } else if (action.equals("kick")) {
                UUID uuid = Player.getUUID(payload.get("nmid").getAsInt());
                if (uuid == null) { return; }
                NetworkManager.getNMPlugin().runSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        NetworkManager.getNMPlugin().kick(uuid, payload.get("reason").getAsString());
                    }
                });
            } else if (action.equals("ban")) {
                UUID uuid = Player.getUUID(payload.get("nmid").getAsInt());
                if (uuid == null) { return; }
                NetworkManager.getNMPlugin().runSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        NetworkManager.getNMPlugin().kick(uuid, payload.get("reason").getAsString());
                        NetworkManager.server.bans.add(new Ban(payload.get("nmid").getAsInt(), (payload.get("banTime").getAsInt() + payload.get("length").getAsInt()), uuid, payload.get("reason").getAsString()));
                    }
                });
            }
        } catch (Exception e) {
            NetworkManager.sentry.sendException(e);
            // TODO Auto-generated catch block
            if (NetworkManager.config.debug)
                e.printStackTrace();
            NetworkManager.LogError("Failed to process payload.");
        }
    }

    public void unregister() {
        NetworkManager.api.Put("/server/" + NetworkManager.GetSID() + "/shutdown");
    }

    public void collectBans() {
        if (NetworkManager.getNMPlugin().isPrimaryThread()) {
            NetworkManager.LogWarning("Can not collect bans on main thread!");
            return;
        }
        JsonObject json = NetworkManager.api.Get("/server/" + NetworkManager.GetSID() + "/bans").json;
        if (json.get("status").getAsString().equals("failure")) {
            return;
        }

        for (int i = 0; i < json.get("bans").getAsJsonArray().size(); i++) {
            JsonObject banjson = json.get("bans").getAsJsonArray().get(i).getAsJsonObject();
            Ban ban = new Ban(banjson.get("uid").getAsInt(), banjson.get("unbanTime").getAsInt(), Player.getUUID(banjson.get("uid").getAsInt(), true), banjson.get("reason").getAsString());
            if (ban.uuid == null) { continue; }
            NetworkManager.getNMPlugin().runSyncTask(new Runnable() {
                @Override
                public void run() {
                    NetworkManager.server.bans.add(ban);
                }
            });
        }
    }
}
