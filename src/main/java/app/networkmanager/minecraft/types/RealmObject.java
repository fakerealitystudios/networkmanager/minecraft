package app.networkmanager.minecraft.types;

import app.networkmanager.minecraft.Realms;

public class RealmObject {
    Realm realms = new Realm();

    public String getDefaultRealm() {
        return Realms.realm;
    }

    public Realm getRealm(String fullRealm) {
        String[] realmSplit = Realms.getRealm(fullRealm).getParts();
        int i = 0;
        int count = realmSplit.length;
        Realm lrealm = realms;
        for (String realmPart : realmSplit) {
            if (!lrealm.containsKey(realmPart)) {
                lrealm.put(realmPart, new Realm());
            }
            i++;
            lrealm = (Realm)lrealm.get(realmPart);
            if (i == count) {
                return lrealm;
            }
        }
        return null;
    }

    public Realm getRealmInclusive(String fullRealm) {
        String[] realmSplit = Realms.getRealm(fullRealm).getParts();
        int i = 0;
        int count = realmSplit.length;
        Realm lrealm = realms;
        Realm result = new Realm();
        for (String realmPart : realmSplit) {
            if (lrealm.containsKey("*")) {
                result.append((Realm)lrealm.get("*"));
            }
            if (!lrealm.containsKey(realmPart)) {
                lrealm.put(realmPart, new Realm());
            }
            i++;
            lrealm = (Realm)lrealm.get(realmPart);
            if (i == count) {
                result.append(lrealm);
            }
        }
        return result;
    }
}
