package app.networkmanager.minecraft.types;

import app.networkmanager.minecraft.Helper;
import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.Realms;
import app.networkmanager.minecraft.events.member.MemberIndexed;
import app.networkmanager.minecraft.events.member.MemberLoaded;
import app.networkmanager.minecraft.events.member.MemberSessionIndexed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.gson.JsonObject;

public class Player extends RealmObject implements Permissions, Data {
    private UUID uuid;
    private String displayName = "Unknown";
    public String sessionDisplayName = null;
    private int nmid = -1;
    private int sessionid = -1;
    private Rank rank;
    private List<Rank> subranks = new ArrayList<>();
    private List<PlayerRank> rankData = new ArrayList<>();
    private boolean justCreated = false;
    private String address = "127.0.0.1";

    public class PlayerRank {
        public int id;
        public Rank rank;
        public String realm;

        public PlayerRank(int id, Rank rank, String realm) {
            this.id = id;
            this.rank = rank;
            this.realm = realm;
        }
    }

    public Player(UUID uuid) {
        this.uuid = uuid;
        this.rank = NetworkManager.ranks.get("user"); // default rank
        registerPlayer("");
    }

    public Player(UUID uuid, String sessionDisplayName) {
        this.uuid = uuid;
        this.rank = NetworkManager.ranks.get("user"); // default rank
        this.sessionDisplayName = sessionDisplayName;
        registerPlayer(sessionDisplayName);
    }

    public UUID getUUID() { return uuid; }

    public int getNMId(){
        return this.nmid;
    }

    public int getSessionId(){
        return this.sessionid;
    }

    public String getDisplayName(){
        return this.displayName;
    }

    public Rank getRank() {
        return this.rank;
    }

    public Rank getRank(String realm) {
        return this.rank;
    }

    public List<Rank> getSubranks() {
        return this.subranks;
    }

    public List<PlayerRank> getRankData() {
        return this.rankData;
    }
    public boolean getJustCreated() { return justCreated; }

    public static UUID getUUID(int nmid) {
        return getUUID(nmid, false);
    }

    public static UUID getUUID(int nmid, boolean lookup) {
        for (Map.Entry<UUID, Player> entry : NetworkManager.players.entrySet()) {
            if (entry.getValue().getNMId() == nmid) {
                return entry.getKey();
            }
        }

        if (lookup) {
            JsonObject json = NetworkManager.api.Get("/member/" + nmid).json;
            if (json.get("status").getAsString().equals("success")) {
                if (json.get("member").getAsJsonObject().get("linked").getAsJsonObject().has("minecraft")) {
                    return UUID.fromString(json.get("member").getAsJsonObject().get("linked").getAsJsonObject().get("minecraft").getAsString());
                }
            }
        }
        return null;
    }

    public void setAddress(String address) {
        if (address.equals("127.0.0.1") || !this.address.equals("127.0.0.1"))
            return;
        this.address = address;
        createSession();
    }

    private void registerPlayer(String sessionDisplayName) {
        NetworkManager.LogDebug("Register Player: " + sessionDisplayName);

        Map<String, String> formData = new HashMap<>();
        formData.put("type", "minecraft");
        formData.put("token", uuid.toString());
        formData.put("displayName", sessionDisplayName);
        JsonObject json = NetworkManager.api.Put("/member", formData).json;
        if (json.get("status").getAsString().equals("failure")) {
            return;
        }
        JsonObject member = json.get("member").getAsJsonObject();
        displayName = member.get("displayName").getAsString();
        this.nmid = member.get("nmid").getAsInt();
        for (int i = 0; i < member.get("permissions").getAsJsonArray().size(); i++) {
            JsonObject perm = member.get("permissions").getAsJsonArray().get(i).getAsJsonObject();
            loadPermission(perm.get("perm").getAsString(), perm.get("realm").getAsString());
        }
        String rankrealm = "*";
        for (int i = 0; i < member.get("ranks").getAsJsonArray().size(); i++) {
            JsonObject rank = member.get("ranks").getAsJsonArray().get(i).getAsJsonObject();
            
            Rank lrank = NetworkManager.ranks.get(rank.get("rank").getAsString());
            if (lrank == null) {
                // Probably should log
                continue;
            }
            String realm = rank.get("realm").getAsString();
            if (!rank.has("id"))
                rank.addProperty("id", 0);
            this.rankData.add(new PlayerRank(rank.get("id").getAsInt(), lrank, realm));
            if (lrank.getSubgrouping() == 0) {
                if (realm.equals(Realms.realm) || (realm.equals("*.minecraft") && rankrealm.equals("*")) || (realm.equals("*") && rankrealm.equals("*"))) {
                    this.rank = lrank;
                    rankrealm = realm;
                }
            } else if (!this.subranks.contains(lrank)) {
                this.subranks.add(lrank);
            }
        }
        rankrealm = null;

        if (this.nmid > 0) {
            NetworkManager.callEvent(new MemberIndexed(this.nmid));
            NetworkManager.callEvent(new MemberLoaded(this));
        } else {
            NetworkManager.LogWarning("Failed to create new network manager id! - 1");
        }
    }

    public void unregisterPlayer() {
        NetworkManager.api.Delete("/member/" + this.nmid + "/session/" + this.sessionid);
        this.sessionid = -1;
    }

    private void createSession() {
        final Player ply = this;
        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                if (ply.sessionid == -1) {
                    Map<String, String> formData = new HashMap<>();
                    formData.put("ip", ply.address);
                    formData.put("displayName", ply.sessionDisplayName);
                    JsonObject json = NetworkManager.api.Put("/member/" + ply.nmid + "/session", formData).json;
                    if (json.get("status").getAsString().equals("failure")) {
                        return;
                    }
                    ply.sessionid = json.get("usid").getAsInt();
                    NetworkManager.callEvent(new MemberSessionIndexed(ply, sessionid));
                } else {
                    Map<String, String> formData = new HashMap<>();
                    formData.put("ip", ply.address);
                    formData.put("displayName", ply.sessionDisplayName);
                    NetworkManager.api.Put("/member/" + ply.nmid + "/session/" + ply.sessionid, formData);
                }
            }
        });
    }

    @Override
    public String getDefaultRealm() {
        return Realms.realm;
    }

    // Permissions interface
    public void insertPermission(String fullPerm, String fullRealm) {

    }

    public void deletePermission(String fullPerm, String fullRealm) {

    }

    @Override
    public List<String> getRealmPermissions(String fullRealm) {
        List<String> perms = Helper.uncheckedCast(this.rank.getRealmInclusive(fullRealm).get("__perms"));
        for (Rank subrank : this.subranks) {
            perms.addAll(Helper.uncheckedCast(subrank.getRealmInclusive(fullRealm).get("__perms")));
        }
        perms.addAll(Helper.uncheckedCast(this.getRealmInclusive(fullRealm).get("__perms")));
        return perms;
    }

    @Override
    public Boolean hasPermission(String fullPerm, String fullRealm) {
        if (fullPerm == null) {
            return null;
        }
        Boolean result = Permissions.super.hasPermission(fullPerm, fullRealm);
        if (result == null) {
            result = this.rank.hasPermission(fullPerm, fullRealm);
        }
        if (result == null) {
            for (Rank subrank : this.subranks) {
                Boolean tmp = subrank.hasPermission(fullPerm, fullRealm);
                if (tmp == null) {
                    //NetworkManager.LogInfo("null subrank");
                } else if (tmp == false) {
                    result = false;
                    //NetworkManager.LogInfo("false subrank");
                    break;
                } else if (tmp == true) {
                    //NetworkManager.LogInfo("true subrank");
                    result = true;
                }
            }
        }
        if (NetworkManager.config.debug)
        {
            boolean bPrint = true;
            for (String str : NetworkManager.config.ignorePermissions) {
                if (str.length() > fullPerm.length()) {
                    continue;
                } else if (str.equals(fullPerm)) {
                    bPrint = false;
                    break;
                } else if (str.endsWith("*") && str.substring(0, str.length()-1).equals(fullPerm.substring(0, str.length()-1))) {
                    bPrint = false;
                    break;
                }
            }
            if (bPrint) {
                if (result == null)
                    NetworkManager.LogDebug(displayName + " perm " + fullPerm + "|" + fullRealm + ": null");
                else
                    NetworkManager.LogDebug(displayName + " perm " + fullPerm + "|" + fullRealm + ": " + result.toString());
            }
        }
        return result;
    }
}
