package app.networkmanager.minecraft.types;

import app.networkmanager.minecraft.Helper;
import app.networkmanager.minecraft.NetworkManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public interface Permissions {
    static final String delimiter = ".";

    default void loadPermission(String fullPerm, String fullRealm) {
        List<String> perms = Helper.uncheckedCast(((RealmObject)this).getRealm(fullRealm).get("__perms"));
        perms.add(fullPerm);
    }

    default void givePermission(String fullPerm) {
        givePermission(fullPerm, ((RealmObject)this).getDefaultRealm());
    }

    default void givePermission(String fullPerm, String fullRealm) {
        List<String> perms = Helper.uncheckedCast(((RealmObject)this).getRealm(fullRealm).get("__perms"));
        if (!perms.contains(fullPerm)) {
            perms.add(fullPerm);
            // Insert to the database
            insertPermission(fullPerm, fullRealm);
        }
    }
    void insertPermission(String fullPerm, String fullRealm);

    default void takePermission(String fullPerm, String fullRealm) {
        List<String> perms = Helper.uncheckedCast(((RealmObject)this).getRealm(fullRealm).get("__perms"));
        if (perms.contains(fullPerm)) {
            perms.remove(fullPerm);
            // Delete from the database
            deletePermission(fullPerm, fullRealm);
        }
    }
    void deletePermission(String fullPerm, String fullRealm);

    default List<String> getRealmPermissions() {
        return getRealmPermissions(((RealmObject)this).getDefaultRealm());
    }

    default List<String> getRealmPermissions(String fullRealm) {
        return Helper.uncheckedCast(((RealmObject)this).getRealmInclusive(fullRealm).get("__perms"));
    }

    default Boolean hasPermission(String fullPerm) {
        return hasPermission(fullPerm, ((RealmObject)this).getDefaultRealm());
    }

    default Boolean hasPermission(String fullPerm, String fullRealm) {
        if (!NetworkManager.permsCache.permissions.contains(fullPerm)) {
            NetworkManager.permsCache.permissions.add(fullPerm);
            NetworkManager.permsCache.writeYaml();
            NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
                @Override
                public void run() {
                    Map<String, String> formData = new HashMap<>();
                    formData.put("perm", fullPerm);
                    formData.put("game", "minecraft");
                    NetworkManager.api.Put("/server/" + NetworkManager.GetSID() + "/permission", formData);
                }
            });
        }

        List<String> realm = Helper.uncheckedCast(((RealmObject)this).getRealmInclusive(fullRealm).get("__perms"));
        String[] permSplit = fullPerm.split(Pattern.quote(delimiter));

        // Check if any perms are negated first, they take priority
        if (realm.contains("!" + fullPerm)) {
            return false;
        } else if (fullPerm.substring(fullPerm.length()-1).equals("%")) {
            String perm = "!" + fullPerm.substring(0, fullPerm.length()-1);
            for (String realmPerm : realm) {
                if (realmPerm.substring(0, perm.length()).equalsIgnoreCase(perm)) {
                    return false;
                }
            }
        } else {
            String perm = "*";
            for (String permPart : permSplit) {
                if (realm.contains("!" + perm)) {
                    return false;
                } else {
                    perm = perm.substring(0, perm.length() - 1);
                    perm += permPart + delimiter + "*";
                }
            }
        }

        // Now check if we do have the perm
        if (realm.contains(fullPerm)) {
            return true;
        } else if (fullPerm.substring(fullPerm.length()-1).equals("%")) {
            String perm = fullPerm.substring(0, fullPerm.length()-1);
            for (String realmPerm : realm) {
                if (realmPerm.substring(0, perm.length()).equalsIgnoreCase(perm)) {
                    return true;
                }
            }
        } else {
            String perm = "*";
            for (String permPart : permSplit) {
                if (realm.contains(perm)) {
                    return true;
                } else {
                    perm = perm.substring(0, perm.length() - 1);
                    perm += permPart + delimiter + "*";
                }
            }
        }
        return null; // null so we know nothing was found
    }
}
