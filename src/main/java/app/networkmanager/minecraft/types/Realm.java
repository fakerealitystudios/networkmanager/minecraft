package app.networkmanager.minecraft.types;

import java.util.*;

public class Realm {
    private HashMap<String, Object> map = new HashMap<>();

    public Realm() {
        map.put("__data", new HashMap<String, Object>());
        map.put("__perms", new ArrayList<String>());
    }

    @SuppressWarnings("all")
    public void append(Realm realm) {
        for (Map.Entry<String, Object> entry : realm.entrySet()) {
            if (!map.containsKey(entry.getKey()))
            {
                if (entry.getValue() instanceof Realm) {
                    this.append((Realm)entry.getValue());
                }
                continue;
            }
            if (entry.getValue() instanceof Realm) {
                append((Realm)entry.getValue());
            } else if (entry.getValue() instanceof List) {
                ((List)map.get(entry.getKey())).addAll((List)entry.getValue());
            }
        }
    }

    public boolean containsKey(String key) {
        return map.containsKey(key);
    }

    public Object get(String key) {
        return map.get(key);
    }

    public Object put(String key, Object value) {
        return map.put(key, value);
    }

    public Set<Map.Entry<String, Object>> entrySet() {
        return map.entrySet();
    }
}
