package app.networkmanager.minecraft.types;

import java.util.UUID;

public class Ban {
    public int nmid;
    public int unbanTime;
    public UUID uuid;
    public String reason;

    public Ban(int nmid, int unbanTime, UUID uuid, String reason) {
        this.nmid = nmid;
        this.unbanTime = unbanTime;
        this.uuid = uuid;
        this.reason = reason;
    }
}
