package app.networkmanager.minecraft;

import app.networkmanager.minecraft.implementations.NetworkManagerPlugin;
import app.networkmanager.minecraft.configuration.MainConfiguration;
import app.networkmanager.minecraft.configuration.PermissionsCache;
import app.networkmanager.minecraft.configuration.ServerConfiguration;
import app.networkmanager.minecraft.events.Event;
import app.networkmanager.minecraft.events.Listener;
import app.networkmanager.minecraft.listeners.NetworkManagerListener;
import app.networkmanager.minecraft.types.Player;
import app.networkmanager.minecraft.types.Rank;
import app.networkmanager.minecraft.types.Server;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.EventBus;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;

import java.util.*;

public class NetworkManager {
    public enum PluginType {
        BUNGEE, VELOCITY,

        BUKKIT, SPONGE,
    }

    public enum LogLevel {
        DEBUG, INFO, WARNING, ERROR,
    }

    private static PluginType type;
    public static SentryClient sentry;
    public static NetworkManager instance;
    public static NetworkManagerPlugin plugin;
    public static MainConfiguration config;
    public static ServerConfiguration config_server;
    public static PermissionsCache permsCache;
    public static API api;
    public static Server server;

    private static Multimap<Object, Listener> Listeners = ArrayListMultimap.create();
    private static EventBus eventBus;

    public static HashMap<UUID, Player> players = new HashMap<>();
    public static HashMap<String, Rank> ranks = new HashMap<>();
    public static HashMap<String, String> settings = new HashMap<>();

    public NetworkManager(NetworkManagerPlugin plugin, PluginType type) {
        NetworkManager.plugin = plugin;
        NetworkManager.type = type;
        NetworkManager.instance = this;
        NetworkManager.sentry = SentryClientFactory.sentryClient("https://f799bf45186d4c979d7d074b1e785cac@o1207037.ingest.sentry.io/6340478?release=minecraft@" + getClass().getPackage().getImplementationVersion() + "&enviroment=production" + "&dist=" + type.toString());
        config = MainConfiguration.initialize();
        config_server = ServerConfiguration.initialize();

        JsonElement jsonElement = new JsonParser().parse(NetworkManager.config_server.settings);
        for (int i = 0; i < jsonElement.getAsJsonArray().size(); i++) {
            JsonObject setting = jsonElement.getAsJsonArray().get(i).getAsJsonObject();
            NetworkManager.settings.put(setting.get("name").getAsString(), setting.get("value").getAsString());
        }
    }

    public static Integer GetSID() {
        if (NetworkManager.config_server.server_id != -1) {
            return NetworkManager.config_server.server_id;
        }
        return -1;
    }

    public boolean onEnable() {
        eventBus = new EventBus();
        permsCache = PermissionsCache.initialize();

        registerListener(this, new NetworkManagerListener());

        ExternalApiRequests.GetInstance().VersionCheck();

        if (NetworkManager.config_server.secret.isEmpty()) {
            LogInfo("Disabling! NetworkManager requires the API secret.");
            return false;
        }

        api = new API();

        server = new Server();
        loadRanks();

        return true;
    }

    public void onDisable() {
        // This is to make sure we perform the log before shutting down
        if (NetworkManager.api != null) {
            app.networkmanager.minecraft.Logs.LogSynchronously(app.networkmanager.minecraft.Logs.LogType.SERVER, "stop");
        }

        // Unregister our own listeners
        unregisterListeners(this);

        for (Player ply : players.values()) {
            ply.unregisterPlayer();
        }
        players.clear(); // Don't remove
        if (server != null) {
            server.unregister();
            server = null;
        }

        api = null;
    }

    public static boolean isOnline(UUID uuid) {
        return getNMPlugin().isOnline(uuid);
    }

    public static void reloadPlayer(UUID uuid) {
        final Player nm_ply = new Player(uuid, players.get(uuid).sessionDisplayName);
        if (nm_ply.getNMId() != -1) {
            // Should overwrite any that are already there
            players.put(uuid, nm_ply);
        }
    }

    public void updatePlayerSession(UUID uuid) {

    }

    public void loadRanks() {
        JsonObject json = (JsonObject) NetworkManager.api.Get("/server/" + NetworkManager.GetSID() + "/ranks").json;
        if (json.get("status").getAsString().equals("failure")) {
            LogError("Failed to load ranks!");
            return;
        }
        LogInfo("Loading " + json.get("ranks").getAsJsonArray().size() + " ranks");
        ranks = new HashMap<>();
        for (int i = 0; i < json.get("ranks").getAsJsonArray().size(); i++) {
            JsonObject jrank = json.get("ranks").getAsJsonArray().get(i).getAsJsonObject();
            Rank rank = new Rank(jrank.get("rank").getAsString(), jrank.get("displayName").getAsString(), jrank.get("subgrouping").getAsInt(), jrank.get("realm").getAsString());
            ranks.put(jrank.get("rank").getAsString(), rank);
            for (int k = 0; k < jrank.get("permissions").getAsJsonArray().size(); k++) {
                JsonObject jperm = jrank.get("permissions").getAsJsonArray().get(k).getAsJsonObject();
                rank.loadPermission(jperm.get("perm").getAsString(), jperm.get("realm").getAsString());
            }
        }
        for (int i = 0; i < json.get("ranks").getAsJsonArray().size(); i++) {
            JsonObject jrank = json.get("ranks").getAsJsonArray().get(i).getAsJsonObject();
            Rank rank = ranks.get(jrank.get("rank").getAsString());
            rank.setInherits(jrank.get("inherit").isJsonNull() ? null : jrank.get("inherit").getAsString());
        }
    }

    public static <T extends Event> T callEvent(T event) {
        if (event == null) {
            LogWarning("Can't have a null event!");
            return null;
        }
        long start = System.nanoTime();
        eventBus.post(event);

        long elapsed = System.nanoTime() - start;
        if ( elapsed > 250000000 )
        {
            NetworkManager.LogWarning( "Event " + event + " took " + elapsed + "ns to process!");
        }
        return event;
    }

    public static void registerListener(Object Plugin, Listener listener) {
        eventBus.register(listener);
        Listeners.put(Plugin, listener);
    }

    public static void unregisterListener(Listener listener) {
        eventBus.unregister(listener);
        Listeners.values().remove(listener);
    }

    public static void unregisterListeners(Object Plugin) {
        for ( Iterator<Listener> it = Listeners.get( Plugin ).iterator(); it.hasNext(); ) {
            eventBus.unregister( it.next() );
            it.remove();
        }
    }

    public static String getSaltString() {
        return getSaltString(10);
    }

    public static String getSaltString(int length) {
        return getSaltString(length, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    public static String getSaltString(int length, String chars) {
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * chars.length());
            salt.append(chars.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public static void LogDebug(String msg) {
        if (NetworkManager.config.debug == false)
            return;
        getNMPlugin().Log(msg, LogLevel.INFO);
    }

    public static void LogHTTP(String msg) {
        if (NetworkManager.config.debugHTTP == false)
            return;
        getNMPlugin().Log(msg, LogLevel.INFO);
    }

    public static void LogInfo(String msg) {
        getNMPlugin().Log(msg, LogLevel.INFO);
    }

    public static void LogWarning(String msg) {
        getNMPlugin().Log(msg, LogLevel.WARNING);
    }

    public static void LogError(String msg) {
        getNMPlugin().Log(msg, LogLevel.ERROR);
    }

    // Fancy things
    public static PluginType getType() {
        return type;
    }

    public static NetworkManagerPlugin getNMPlugin() {
        return plugin;
    }
}
