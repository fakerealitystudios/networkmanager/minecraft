package app.networkmanager.minecraft.implementations.sponge;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.NetworkManager.LogLevel;
import app.networkmanager.minecraft.implementations.NetworkManagerPlugin;
import app.networkmanager.minecraft.implementations.sponge.listeners.PlayerListener;
import app.networkmanager.minecraft.types.Player;

import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.Game;
import org.spongepowered.api.Server;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;

// https://docs.spongepowered.org/stable/en/plugin/lifecycle.html
import org.spongepowered.api.event.game.state.GameConstructionEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameAboutToStartServerEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.event.game.state.GameStoppedServerEvent;

import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;

// Imports for logger
import com.google.inject.Inject;
import org.slf4j.Logger;

@Plugin(id = "networkmanager", name = "Network Manager", url = "https://www.networkmanager.io/", authors = {"Montana Tuska"},
    version = "1.0.0", description = "Connects to a NetworkManager API for administration, logging, and more.")
public class Implementation implements NetworkManagerPlugin {
    private static Implementation instance;
    @Inject
    private Logger logger;

    // 
    @Listener
    public void onServerConstruction(GameConstructionEvent event) {
        instance = this;
    }
    
    // During this state, the plugin should finish any work needed in order to be functional. Global event handlers should get registered in this stage.
    @Listener
    public void onServerInitilization(GameInitializationEvent event) {
        new NetworkManager(this, NetworkManager.PluginType.SPONGE);
        Sponge.getEventManager().registerListeners(this, new PlayerListener());
    }

    // The server instance exists, but worlds are not yet loaded.
    @Listener
    public void onServerAboutToStart(GameAboutToStartServerEvent event) {
        
    }

    // The server instance exists, and worlds are loaded. Command registration is handled during this state.
    @Listener
    public void onServerStarting(GameStartingServerEvent event) {
        
    }

    // The server instance exists, and worlds are loaded.
    @Listener
    public void onServerStarted(GameStartedServerEvent event) {
        
    }

    // This state occurs immediately before the final tick, before the worlds are saved.
    @Listener
    public void onServerStopping(GameStoppingServerEvent event) {
        
    }

    // During this state, no players are connected and no changes to worlds are saved.
    @Listener
    public void onServerStopped(GameStoppedServerEvent event) {
        
    }

    public static Implementation getInstance() {
        return instance;
    }
    
    public Server getServer() {
        return Sponge.getServer();
    }

    public Game getGame() {
        return Sponge.getGame();
    }

    @Override
    public Object getPluginServer() {
        return Sponge.getServer();
    }

    @Override
    public void Reload() {

    }

    @Override
    public String getIp() {
        try {
            return Sponge.getServer().getBoundAddress().get().getAddress().getHostAddress();
        } catch(NoSuchElementException e) {}
        return "127.0.0.1";
    }

    @Override
    public int getPort() {
        try {
            return Sponge.getServer().getBoundAddress().get().getPort();
        } catch(NoSuchElementException e) {}
        return 25565;
    }

    @Override
    public boolean isPrimaryThread() {
        return Sponge.getServer().isMainThread();
    }

    @Override
    public void scheduleRepeatingAsyncTask(Runnable runnable, Long delay, Long period) {
        Sponge.getScheduler().createAsyncExecutor(this).scheduleWithFixedDelay(runnable, delay/20L, period/20L, TimeUnit.SECONDS);
    }

    @Override
    public void scheduleAsyncTask(Runnable runnable, Long delay) {
        Sponge.getScheduler().createAsyncExecutor(this).schedule(runnable, delay/20L, TimeUnit.SECONDS);
    }

    @Override
    public void runAsyncTask(Runnable runnable) {
        Sponge.getScheduler().createAsyncExecutor(this).execute(runnable);
    }

    @Override
    public void scheduleRepeatingSyncTask(Runnable runnable, Long delay, Long period) {
        Sponge.getScheduler().createSyncExecutor(this).scheduleWithFixedDelay(runnable, delay/20L, period/20L, TimeUnit.SECONDS);
    }

    @Override
    public void scheduleSyncTask(Runnable runnable, Long delay) {
        Sponge.getScheduler().createSyncExecutor(this).schedule(runnable, delay/20L, TimeUnit.SECONDS);
    }

    @Override
    public void runSyncTask(Runnable runnable) {
        Sponge.getScheduler().createSyncExecutor(this).execute(runnable);
    }

    @Override
    public void Log(String msg, LogLevel level) {
        if (level == LogLevel.DEBUG)
            logger.debug(msg);
        else if (level == LogLevel.INFO)
            logger.info(msg);
        else if (level == LogLevel.WARNING)
            logger.warn(msg);
        else if (level == LogLevel.ERROR)
            logger.error(msg);
    }

    @Override
    public void dispatchCommand(String cmd) {
        //Sponge.getCommandManager().process(null, arguments)
    }

    @Override
    public void broadcastMessage(String msg) {
        
    }

    @Override
    public List<Player> matchPlayer(String player) {
        return new ArrayList();
    }

    @Override
    public boolean isOnline(UUID uuid) {
        return Sponge.getServer().getPlayer(uuid).isPresent();
    }

    @Override
    public void kick(UUID uuid, String reason) {
        try {
            Sponge.getServer().getPlayer(uuid).get().kick(Text.of(reason));
        } catch(NoSuchElementException e) {}
    }
}