package app.networkmanager.minecraft.implementations.sponge.listeners;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.Text;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.types.Player;

public class PlayerListener {
    @Listener
    public void onClientConnection(ClientConnectionEvent.Login e) {
        final Player nm_ply = new Player(e.getTargetUser().getUniqueId(), e.getTargetUser().getName());
        nm_ply.setAddress(e.getConnection().getAddress().getAddress().getHostAddress());
        if (nm_ply.getNMId() == -1) {
            e.setCancelled(true);
            e.setMessage(Text.of("BungeeCord: Failed to load NetworkManager data!"));
            return;
        }
        NetworkManager.players.put(e.getTargetUser().getUniqueId(), nm_ply);
    }

    @Listener
    public void onClientDisconnect(ClientConnectionEvent.Disconnect e) {
        NetworkManager.players.get(e.getTargetEntity().getUniqueId()).unregisterPlayer();
        NetworkManager.players.remove(e.getTargetEntity().getUniqueId());
    }
}