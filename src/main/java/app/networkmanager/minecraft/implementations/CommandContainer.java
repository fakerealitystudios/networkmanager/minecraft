package app.networkmanager.minecraft.implementations;

import java.util.HashMap;

import app.networkmanager.minecraft.commands.BaseCommand;

public class CommandContainer {
    private static HashMap<String, BaseCommand> commandMap = new HashMap<>();

    public static void addComand(BaseCommand s)
    {
        commandMap.put(s.getCommand(), s);
    }

    public static HashMap<String, BaseCommand> getCommands()
    {
        return commandMap;
    }
}