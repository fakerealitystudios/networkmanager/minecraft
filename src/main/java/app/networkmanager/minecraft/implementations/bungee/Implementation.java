package app.networkmanager.minecraft.implementations.bungee;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.NetworkManager.LogLevel;
import app.networkmanager.minecraft.implementations.NetworkManagerPlugin;
import app.networkmanager.minecraft.implementations.bungee.listeners.PlayerListener;
import app.networkmanager.minecraft.types.Player;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;

public class Implementation extends Plugin implements NetworkManagerPlugin {
    private static Implementation instance;

    @Override
    public void onLoad() {
        instance = this;

        new NetworkManager(this, NetworkManager.PluginType.BUNGEE);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onEnable() {
        if (!getProxy().getConfig().isOnlineMode()) {
            NetworkManager.LogWarning("Offline mode is not explicitly supported! Use with care!");
        }

        NetworkManager.LogInfo("Enabling NetworkManager");

        if (!NetworkManager.instance.onEnable()) {
            this.onDisable();
            return;
        }

        getProxy().getPluginManager().registerListener(this, new PlayerListener());
    }

    @Override
    public void onDisable() {
        getProxy().getScheduler().cancel(instance);

        NetworkManager.instance.onDisable();
    }

    public static Implementation getInstance() {
        return instance;
    }

    @Override
    public Object getPluginServer() {
        return getProxy();
    }

    @Override
    public void Reload() {

    }

    @Override
    public String getIp() {
        return "127.0.0.1";
    }

    @Override
    public int getPort() {
        return 25565;
    }

    @Override
    public boolean isPrimaryThread() {
        return false;
    }

    @Override
    public void scheduleRepeatingAsyncTask(Runnable runnable, Long delay, Long period) {
        getProxy().getScheduler().schedule(this, runnable, delay/20L, period/20L, TimeUnit.SECONDS);
    }

    @Override
    public void scheduleAsyncTask(Runnable runnable, Long delay) {
        getProxy().getScheduler().schedule(this, runnable, delay/20L, TimeUnit.SECONDS);
    }

    @Override
    public void runAsyncTask(Runnable runnable) {
        getProxy().getScheduler().runAsync(this, runnable);
    }

    // These are technically ASYNC tasks! beware beware!
    @Override
    public void scheduleRepeatingSyncTask(Runnable runnable, Long delay, Long period) {
        getProxy().getScheduler().schedule(this, runnable, delay/20L, period/20L, TimeUnit.SECONDS);
    }

    @Override
    public void scheduleSyncTask(Runnable runnable, Long delay) {
        getProxy().getScheduler().schedule(this, runnable, delay/20L, TimeUnit.SECONDS);
    }

    @Override
    public void runSyncTask(Runnable runnable) {
        runnable.run();
    }

    @Override
    public void Log(String msg, LogLevel level) {
        if (level == LogLevel.DEBUG)
            getLogger().fine(msg);
        else if (level == LogLevel.INFO)
            getLogger().info(msg);
        else if (level == LogLevel.WARNING)
            getLogger().warning(msg);
        else if (level == LogLevel.ERROR)
            getLogger().severe(msg);
    }

    @Override
    public void dispatchCommand(String cmd) {
        getProxy().getPluginManager().dispatchCommand(getProxy().getConsole(), cmd);
    }

    @Override
    public void broadcastMessage(String msg) {
        
    }

    @Override
    public List<Player> matchPlayer(String player) {
        return new ArrayList();
    }

    @Override
    public boolean isOnline(UUID uuid) {
        return getProxy().getPlayer(uuid) != null;
    }

    @Override
    public void kick(UUID uuid, String reason) {
        getProxy().getPlayer(uuid).disconnect(new TextComponent(reason));
    }
}
