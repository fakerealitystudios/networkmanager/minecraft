package app.networkmanager.minecraft.implementations.bungee.listeners;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.types.Player;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener {
    @EventHandler
    public void onLogin(LoginEvent e) {
        final Player nm_ply = new Player(e.getConnection().getUniqueId(), e.getConnection().getName());
        nm_ply.setAddress(e.getConnection().getSocketAddress().toString());
        if (nm_ply.getNMId() == -1) {
            e.setCancelReason(new TextComponent("BungeeCord: Failed to load NetworkManager data!"));
            e.setCancelled(true);
            return;
        }
        NetworkManager.players.put(e.getConnection().getUniqueId(), nm_ply);
    }

    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent e) {
        NetworkManager.players.get(e.getPlayer().getUniqueId()).unregisterPlayer();
        NetworkManager.players.remove(e.getPlayer().getUniqueId());
    }
}
