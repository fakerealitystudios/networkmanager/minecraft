package app.networkmanager.minecraft.implementations.bukkit;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.NetworkManager.LogLevel;
import app.networkmanager.minecraft.implementations.NetworkManagerPlugin;
import app.networkmanager.minecraft.commands.*;
import app.networkmanager.minecraft.implementations.bukkit.commands.*;
import app.networkmanager.minecraft.implementations.bukkit.listeners.PluginMessageListener;
import app.networkmanager.minecraft.implementations.bukkit.listeners.LogListener;
import app.networkmanager.minecraft.implementations.bukkit.listeners.PlayerListener;
import app.networkmanager.minecraft.types.Player;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;

import java.util.UUID;
import java.util.List;
import java.util.ArrayList;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class Implementation extends JavaPlugin implements NetworkManagerPlugin {
    private static Implementation instance;
    public Vault vault;

    @Override
    public void onLoad() {
        instance = this;
        new NetworkManager(this, NetworkManager.PluginType.BUKKIT);
    }

    @Override
    public void onEnable() {
        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            if (vault == null) {
                vault = new Vault();
            }
            vault.setupVault();
        }

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new PluginMessageListener());

        getCommand("nm").setExecutor(new CommandExecutor());
        getCommand("nm").setTabCompleter(new TabCompleter());

        CommandExecutor.addComand(new HelpCommand());
        CommandExecutor.addComand(new InfoCommand());
        CommandExecutor.addComand(new PanelCommand());
        CommandExecutor.addComand(new RankMultiCommand());
        CommandExecutor.addComand(new DebugMultiCommand());
        CommandExecutor.addComand(new BanCommand());
        CommandExecutor.addComand(new ReloadCommand());
        CommandExecutor.addComand(new BanCommand());
        CommandExecutor.addComand(new RealmBanCommand());
        CommandExecutor.addComand(new WarnCommand());
        CommandExecutor.addComand(new PunishCommand());

        // Add events before anything occurs
        getServer().getPluginManager().registerEvents(new LogListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        // Perform the actual setup
        if (!NetworkManager.instance.onEnable()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // If there are players online, asynchronously pull their data
        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                for (org.bukkit.entity.Player ply : org.bukkit.Bukkit.getOnlinePlayers())
                {
                    try {
                        Player nm_ply = new Player(ply.getUniqueId(), ply.getName());
                        NetworkManager.players.put(ply.getUniqueId(), nm_ply);

                        NMPermissible permissible = new NMPermissible(ply, nm_ply, Implementation.instance);

                        // Inject into the player
                        PermissibleInjector.inject(ply, permissible);

                        NetworkManager.LogInfo("Asynchronously reloaded nmid: " + nm_ply.getNMId());
                        
                        if (NetworkManager.config.bungeecord == false) {
                            nm_ply.setAddress(ply.getAddress().getAddress().getHostAddress());
                        } else {
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("IP");
                            ply.sendPluginMessage(Implementation.instance, "BungeeCord", out.toByteArray());
                        }
                    } catch(Exception e) {
                        NetworkManager.sentry.sendException(e);
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onDisable() {
        getServer().getScheduler().cancelTasks(instance);

        if (vault != null) {
            vault.removeVault();
        }

        for (org.bukkit.entity.Player ply : Implementation.instance.getServer().getOnlinePlayers()) {
            try {
                PermissibleInjector.unInject(ply, true);
            } catch (Exception e) {
                NetworkManager.sentry.sendException(e);
                e.printStackTrace();
            }
        }

        NetworkManager.instance.onDisable();
    }

    public static Implementation getInstance() {
        return instance;
    }

    @Override
    public Object getPluginServer() {
        return getServer();
    }

    @Override
    public void Reload() {
        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                for (org.bukkit.entity.Player ply : org.bukkit.Bukkit.getOnlinePlayers())
                {
                    try {
                        Player nm_ply = new Player(ply.getUniqueId(), ply.getName());
                        NetworkManager.players.put(ply.getUniqueId(), nm_ply);
                        NetworkManager.LogDebug("Asynchronously reloaded nmid: " + nm_ply.getNMId());

                        if (NetworkManager.config.bungeecord == false) {
                            nm_ply.setAddress(ply.getAddress().getAddress().getHostAddress());
                        } else {// We'll assume that, if offline, we're running with BungeeCord(if not, welp, oh well. Maybe don't run a cracked server?)
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("IP");
                            ply.sendPluginMessage(Implementation.instance, "BungeeCord", out.toByteArray());
                        }
                    } catch(Exception e) {
                        NetworkManager.sentry.sendException(e);
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public String getIp() {
        return getServer().getIp();
    }

    @Override
    public int getPort() {
        return getServer().getPort();
    }

    @Override
    public boolean isPrimaryThread() {
        return getServer().isPrimaryThread();
    }

    @Override
    public void scheduleRepeatingAsyncTask(Runnable runnable, Long delay, Long period) {
        getServer().getScheduler().runTaskTimerAsynchronously(this, runnable, delay, period);
    }

    @Override
    public void scheduleAsyncTask(Runnable runnable, Long delay) {
        getServer().getScheduler().runTaskLaterAsynchronously(this, runnable, delay);
    }

    @Override
    public void runAsyncTask(Runnable runnable) {
        getServer().getScheduler().runTaskAsynchronously(this, runnable);
    }

    @Override
    public void scheduleRepeatingSyncTask(Runnable runnable, Long delay, Long period) {
        getServer().getScheduler().runTaskTimer(this, runnable, delay, period);
    }

    @Override
    public void scheduleSyncTask(Runnable runnable, Long delay) {
        getServer().getScheduler().runTaskLater(this, runnable, delay);
    }

    @Override
    public void runSyncTask(Runnable runnable) {
        getServer().getScheduler().runTask(this, runnable);
    }

    @Override
    public void Log(String msg, LogLevel level) {
        if (level == LogLevel.DEBUG)
            getLogger().fine(msg);
        else if (level == LogLevel.INFO)
            getLogger().info(msg);
        else if (level == LogLevel.WARNING)
            getLogger().warning(msg);
        else if (level == LogLevel.ERROR)
            getLogger().severe(msg);
    }

    @Override
    public void dispatchCommand(String cmd) {
        getServer().dispatchCommand(getServer().getConsoleSender(), cmd);
    }

    @Override
    public void broadcastMessage(String msg) {
        Bukkit.broadcastMessage(msg);
    }

    @Override
    public List<Player> matchPlayer(String player) {
        List<org.bukkit.entity.Player> bukkit_plys = Bukkit.getServer().matchPlayer(player);
        List<Player> plys = new ArrayList();
        for (org.bukkit.entity.Player bukkit_ply : bukkit_plys) {
            Player ply = NetworkManager.players.get(bukkit_ply.getUniqueId());
            if (ply != null) {
                plys.add(ply);
            }
        }
        return plys;
    }

    @Override
    public boolean isOnline(UUID uuid) {
        return getServer().getPlayer(uuid) != null;
    }

    @Override
    public void kick(UUID uuid, String reason) {
        getServer().getPlayer(uuid).kickPlayer(reason);
    }
}
