package app.networkmanager.minecraft.implementations.bukkit.commands;

import app.networkmanager.minecraft.commands.CommandArgument;
import app.networkmanager.minecraft.commands.CommandSender;
import app.networkmanager.minecraft.commands.CommandArgument.ArgType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WarnCommand extends PunishCommand {
    public String getCommand() { return "warn"; }

    public String getHelp() { return "Adds a warning to the player."; }

    public List<CommandArgument> getArguments() {
        List<CommandArgument> args = new ArrayList<>();
        args.add(new CommandArgument(ArgType.PLAYER, "<player>"));
        args.add(new CommandArgument(ArgType.STRING, "<reason>"));
        return args;
    };

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.punish");
    }

    // nm warn <player> <reason>
    public void onCommand(CommandSender sender, List<String> args)
    {
        List<String> newargs = new ArrayList<>();
        newargs.add(args.get(0)); // Player is first argument
        args.remove(0);
        newargs.add("warning");
        newargs.addAll(args);
        super.onCommand(sender, args);
    }
}