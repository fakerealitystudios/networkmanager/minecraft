package app.networkmanager.minecraft.implementations.bukkit.listeners;

import app.networkmanager.minecraft.NetworkManager;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import org.bukkit.entity.Player;

public class PluginMessageListener implements org.bukkit.plugin.messaging.PluginMessageListener {
    @Override
    public void onPluginMessageReceived(String channel, Player ply, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();
        if (subchannel.equals("IP")) {
            final app.networkmanager.minecraft.types.Player nm_ply = NetworkManager.players.get(ply.getUniqueId());
            nm_ply.setAddress(in.readUTF());
        }
    }
}
