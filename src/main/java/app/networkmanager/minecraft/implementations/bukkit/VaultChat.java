package app.networkmanager.minecraft.implementations.bukkit;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;

import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.types.Rank;

public class VaultChat extends Chat {

    public VaultChat(Permission perms) {
        super(perms);
    }

    @Override
    public String getName() {
        return "NetworkManager";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    // Should lookup by name, uuid, whatever else
    protected app.networkmanager.minecraft.types.Player GetNMPlayer(String player) {
        List<org.bukkit.entity.Player> plys = Implementation.getInstance().getServer().matchPlayer(player);
        if (plys.size() == 1) {
            return NetworkManager.players.get(plys.get(0).getUniqueId());
        }
        return null;
    }

    protected app.networkmanager.minecraft.types.Player GetNMPlayer(OfflinePlayer player) {
        return NetworkManager.players.get(player.getUniqueId());
    }

    @Deprecated
    @Override
    public String getPlayerPrefix(String world, String player) {
        return getGroupPrefix(world, getPrimaryGroup(world, player));
    }
    
    @Override
    public String getPlayerPrefix(String world, OfflinePlayer player) {
    	return getPlayerPrefix(world, player.getName());
    }

    @Deprecated
    @Override
    public String getPlayerPrefix(World world, String player) {
        return getPlayerPrefix(world.getName(), player);
    }
    
    @Override
    public String getPlayerPrefix(Player player) {
        return getPlayerPrefix(player.getWorld().getName(), player);
    }

    @Deprecated
    @Override
    public void setPlayerPrefix(String world, String player, String prefix) {

    }
    
    @Override
    public void setPlayerPrefix(String world, OfflinePlayer player, String prefix) {
    	setPlayerPrefix(world, player.getName(), prefix);
    }

    @Deprecated
    @Override
    public void setPlayerPrefix(World world, String player, String prefix) {
        setPlayerPrefix(world.getName(), player, prefix);
    }
    
    @Override
    public void setPlayerPrefix(Player player, String prefix) {
        setPlayerPrefix(player.getWorld().getName(), player, prefix);
    }

    @Deprecated
    @Override
    public String getPlayerSuffix(String world, String player) {
        return "";
    }
    
    @Override
    public String getPlayerSuffix(String world, OfflinePlayer player) {
    	return getPlayerSuffix(world, player.getName());
    }

    @Deprecated
    @Override
    public String getPlayerSuffix(World world, String player) {
        return getPlayerSuffix(world.getName(), player);
    }
    
    @Override
    public String getPlayerSuffix(Player player) {
        return getPlayerSuffix(player.getWorld().getName(), player);
    }

    @Deprecated
    @Override
    public void setPlayerSuffix(String world, String player, String suffix) {

    }
    
    @Override
    public void setPlayerSuffix(String world, OfflinePlayer player, String suffix) {
    	setPlayerSuffix(world, player.getName(), suffix);
    }
    
    @Deprecated
    @Override
    public void setPlayerSuffix(World world, String player, String suffix) {
        setPlayerSuffix(world.getName(), player, suffix);
    }
    
    @Override
    public void setPlayerSuffix(Player player, String suffix) {
        setPlayerSuffix(player.getWorld().getName(), player, suffix);
    }
    
    @Override
    public String getGroupPrefix(String world, String group) {
        Rank rank = NetworkManager.ranks.get(group);
        if (rank != null) {
            return "[" + rank.getDisplayName() + "]";
        }
        return "";
    }
    
    @Override
    public String getGroupPrefix(World world, String group) {
        return getGroupPrefix(world.getName(), group);
    }
    
    @Override
    public void setGroupPrefix(String world, String group, String prefix) {

    }
    
    @Override
    public void setGroupPrefix(World world, String group, String prefix) {
        setGroupPrefix(world.getName(), group, prefix);
    }
    
    @Override
    public String getGroupSuffix(String world, String group) {
        return "";
    }
    
    @Override
    public String getGroupSuffix(World world, String group) {
        return getGroupSuffix(world.getName(), group);
    }
    
    @Override
    public void setGroupSuffix(String world, String group, String suffix) {

    }
    
    @Override
    public void setGroupSuffix(World world, String group, String suffix) {
        setGroupSuffix(world.getName(), group, suffix);
    }
    
    @Override
    public int getPlayerInfoInteger(String world, OfflinePlayer player, String node, int defaultValue) {
    	return getPlayerInfoInteger(world, player.getName(), node, defaultValue);
    }

	@Deprecated
    @Override
    public int getPlayerInfoInteger(String world, String player, String node, int defaultValue) {
        return 0;
    }

    @Deprecated
    @Override
    public int getPlayerInfoInteger(World world, String player, String node, int defaultValue) {
        return getPlayerInfoInteger(world.getName(), player, node, defaultValue);
    }
    
    @Override
    public int getPlayerInfoInteger(Player player, String node, int defaultValue) {
        return getPlayerInfoInteger(player.getWorld().getName(), player, node, defaultValue);
    }
    
    @Override
    public void setPlayerInfoInteger(String world, OfflinePlayer player, String node, int value) {
    	setPlayerInfoInteger(world, player.getName(), node, value);
    }
    
    @Deprecated
    @Override
    public void setPlayerInfoInteger(String world, String player, String node, int value) {

    }

    @Deprecated
    @Override
    public void setPlayerInfoInteger(World world, String player, String node, int value) {
        setPlayerInfoInteger(world.getName(), player, node, value);
    }
    
    @Override
    public void setPlayerInfoInteger(Player player, String node, int value) {
        setPlayerInfoInteger(player.getWorld().getName(), player, node, value);
    }
    
    @Override
    public int getGroupInfoInteger(String world, String group, String node, int defaultValue) {
        return 0;
    }
    
    @Override
    public int getGroupInfoInteger(World world, String group, String node, int defaultValue) {
        return getGroupInfoInteger(world.getName(), group, node, defaultValue);
    }
    
    @Override
    public void setGroupInfoInteger(String world, String group, String node, int value) {

    }
    
    @Override
    public void setGroupInfoInteger(World world, String group, String node, int value) {
        setGroupInfoInteger(world.getName(), group, node, value);
    }
    
    @Override
    public double getPlayerInfoDouble(String world, OfflinePlayer player, String node, double defaultValue) {
    	return getPlayerInfoDouble(world, player.getName(), node, defaultValue);
    }
    
    @Deprecated
    @Override
    public double getPlayerInfoDouble(String world, String player, String node, double defaultValue) {
        return 0;
    }

    @Deprecated
    @Override
    public double getPlayerInfoDouble(World world, String player, String node, double defaultValue) {
        return getPlayerInfoDouble(world.getName(), player, node, defaultValue);
    }
    
    @Override
    public double getPlayerInfoDouble(Player player, String node, double defaultValue) {
        return getPlayerInfoDouble(player.getWorld().getName(), player, node, defaultValue);
    }
    
    @Override
    public void setPlayerInfoDouble(String world, OfflinePlayer player, String node, double value) {
    	setPlayerInfoDouble(world, player.getName(), node, value);
    }
    
    @Deprecated
    @Override
    public void setPlayerInfoDouble(String world, String player, String node, double value) {

    }

    @Deprecated
    @Override
    public void setPlayerInfoDouble(World world, String player, String node, double value) {
        setPlayerInfoDouble(world.getName(), player, node, value);
    }
    
    @Override
    public void setPlayerInfoDouble(Player player, String node, double value) {
        setPlayerInfoDouble(player.getWorld().getName(), player, node, value);
    }
    
    @Override
    public double getGroupInfoDouble(String world, String group, String node, double defaultValue) {
        return 0;
    }
    
    @Override
    public double getGroupInfoDouble(World world, String group, String node, double defaultValue) {
        return getGroupInfoDouble(world.getName(), group, node, defaultValue);
    }
    
    @Override
    public void setGroupInfoDouble(String world, String group, String node, double value) {

    }
    
    @Override
    public void setGroupInfoDouble(World world, String group, String node, double value) {
        setGroupInfoDouble(world.getName(), group, node, value);
    }
    
    @Override
    public boolean getPlayerInfoBoolean(String world, OfflinePlayer player, String node, boolean defaultValue) {
    	return getPlayerInfoBoolean(world, player.getName(), node, defaultValue);
    }
    
    @Deprecated
    @Override
    public boolean getPlayerInfoBoolean(String world, String player, String node, boolean defaultValue) {
        return false;
    }

    @Deprecated
    @Override
    public boolean getPlayerInfoBoolean(World world, String player, String node, boolean defaultValue) {
        return getPlayerInfoBoolean(world.getName(), player, node, defaultValue);
    }
    
    @Override
    public boolean getPlayerInfoBoolean(Player player, String node, boolean defaultValue) {
        return getPlayerInfoBoolean(player.getWorld().getName(), player, node, defaultValue);
    }
    
    @Override
    public void setPlayerInfoBoolean(String world, OfflinePlayer player, String node, boolean value) {
    	setPlayerInfoBoolean(world, player.getName(), node, value);
    }
    
    @Deprecated
    @Override
    public void setPlayerInfoBoolean(String world, String player, String node, boolean value) {

    }

    @Deprecated
    @Override
    public void setPlayerInfoBoolean(World world, String player, String node, boolean value) {
        setPlayerInfoBoolean(world.getName(), player, node, value);
    }
    
    @Override
    public void setPlayerInfoBoolean(Player player, String node, boolean value) {
        setPlayerInfoBoolean(player.getWorld().getName(), player, node, value);
    }
    
    @Override
    public boolean getGroupInfoBoolean(String world, String group, String node, boolean defaultValue) {
        return false;
    }
    
    @Override
    public boolean getGroupInfoBoolean(World world, String group, String node, boolean defaultValue) {
        return getGroupInfoBoolean(world.getName(), group, node, defaultValue);
    }
    
    @Override
    public void setGroupInfoBoolean(String world, String group, String node, boolean value) {

    }
    
    @Override
    public void setGroupInfoBoolean(World world, String group, String node, boolean value) {
        setGroupInfoBoolean(world.getName(), group, node, value);
    }
    
    @Override
    public String getPlayerInfoString(String world, OfflinePlayer player, String node, String defaultValue) {
    	return getPlayerInfoString(world, player.getName(), node, defaultValue);
    }

    @Deprecated
    @Override
    public String getPlayerInfoString(String world, String player, String node, String defaultValue) {
        return "";
    }

    @Deprecated
    @Override
    public String getPlayerInfoString(World world, String player, String node, String defaultValue) {
        return getPlayerInfoString(world.getName(), player, node, defaultValue);
    }

    @Override
    public String getPlayerInfoString(Player player, String node, String defaultValue) {
        return getPlayerInfoString(player.getWorld().getName(), player, node, defaultValue);
    }
    
    @Override
    public void setPlayerInfoString(String world, OfflinePlayer player, String node, String value) {
    	setPlayerInfoString(world, player.getName(), node, value);
    }

    @Deprecated
    @Override
    public void setPlayerInfoString(String world, String player, String node, String value) {

    }

    @Deprecated
    @Override
    public void setPlayerInfoString(World world, String player, String node, String value) {
        setPlayerInfoString(world.getName(), player, node, value);
    }
    
    @Override
    public void setPlayerInfoString(Player player, String node, String value) {
        setPlayerInfoString(player.getWorld().getName(), player, node, value);
    }
    
    @Override
    public String getGroupInfoString(String world, String group, String node, String defaultValue) {
        return "";
    }
    
    @Override
    public String getGroupInfoString(World world, String group, String node, String defaultValue) {
        return getGroupInfoString(world.getName(), group, node, defaultValue);
    }
    
    @Override
    public void setGroupInfoString(String world, String group, String node, String value) {
        
    }
    
    @Override
    public void setGroupInfoString(World world, String group, String node, String value) {
        setGroupInfoString(world.getName(), group, node, value);
    }

}
