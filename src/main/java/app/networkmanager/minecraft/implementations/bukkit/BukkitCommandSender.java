package app.networkmanager.minecraft.implementations.bukkit;

import java.util.UUID;

import org.bukkit.entity.Player;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.commands.CommandSender;

public class BukkitCommandSender extends CommandSender {
    private org.bukkit.command.CommandSender sender;

    public BukkitCommandSender(org.bukkit.command.CommandSender sender) {
        this.sender = sender;
    }

    @Override
    public Object getInstance() {
        return sender;
    }

    @Override
    public void sendMessage(String msg) {
        sender.sendMessage(msg);
    }

    @Override
    public boolean hasPermission(String fullPerm) {
        return sender.hasPermission(fullPerm);
    }

    @Override
    public boolean isPlayer() {
        return sender instanceof Player;
    }

    @Override
    public app.networkmanager.minecraft.types.Player getNMPlayer() {
        return NetworkManager.players.get(getUniqueId());
    }

    @Override
    public UUID getUniqueId() {
        if (!isPlayer())
            return null;
        return ((Player)sender).getUniqueId();
    }
    
}