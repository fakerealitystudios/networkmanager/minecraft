package app.networkmanager.minecraft.implementations.bukkit;

import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;

import net.milkbowl.vault.permission.Permission;
import net.milkbowl.vault.chat.Chat;

import app.networkmanager.minecraft.NetworkManager;

public class Vault {

    public static VaultPermissions VaultPerms;
    public static VaultChat VaultChat;

    public void setupVault() {
        NetworkManager.LogInfo("Found Vault! Creating permissions service...");

        try {
            if (VaultPerms == null) {
                VaultPerms = new VaultPermissions();
                if (VaultChat == null) {
                    VaultChat = new VaultChat(VaultPerms);
                }
            }

            final ServicesManager sm = Implementation.getInstance().getServer().getServicesManager();
            sm.register(Permission.class, VaultPerms, Implementation.getInstance(), ServicePriority.High);
            sm.register(Chat.class, VaultChat, Implementation.getInstance(), ServicePriority.High);
        } catch(Exception e) {
            NetworkManager.sentry.sendException(e);
            e.printStackTrace();
        }
    }

    public void removeVault() {
        final ServicesManager sm = Implementation.getInstance().getServer().getServicesManager();
        if (VaultPerms != null) {
            sm.unregister(Permission.class, VaultPerms);
            VaultPerms = null;
        }
        if (VaultChat != null) {
            sm.unregister(Chat.class, VaultChat);
            VaultChat = null;
        }
    }

}