package app.networkmanager.minecraft.implementations.bukkit;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.implementations.CommandContainer;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandExecutor extends CommandContainer implements org.bukkit.command.CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length >= 1)
        {
            if (getCommands().containsKey(args[0]))
            {
                List<String> argsList = new ArrayList<>(Arrays.asList(args));
                argsList.remove(0);
                try {
                    getCommands().get(args[0]).runCommand(new BukkitCommandSender(sender), argsList);
                } catch (Exception e) {
                    NetworkManager.sentry.sendException(e);
                    if (NetworkManager.config.debug) {
                        e.printStackTrace();
                    } else {
                        NetworkManager.LogError("Command '" + args[0] + "' failed! Enable debug to see stacktrace next time!");
                    }
                }
            }
            else
            {
                sender.sendMessage("This command does not exist!");
                return false;
            }
        }
        else
        {
            Bukkit.dispatchCommand(sender, "nm info");
        }

        return true;
    }
}
