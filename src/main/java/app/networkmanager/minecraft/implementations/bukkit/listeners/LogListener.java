package app.networkmanager.minecraft.implementations.bukkit.listeners;

import app.networkmanager.minecraft.Logs;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.*;

import java.util.ArrayList;
import java.util.List;

public class LogListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent e) {
        List<Object> details = new ArrayList<>();
        details.add(e.getPlayer());
        Logs.Log(Logs.LogType.PLAYER, "connect", details);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLeave(PlayerQuitEvent e) {
        List<Object> details = new ArrayList<>();
        details.add(e.getPlayer());
        Logs.Log(Logs.LogType.PLAYER, "disconnect", details);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        if (e.isCancelled()) return;
        List<Object> details = new ArrayList<>();
        details.add(e.getPlayer());
        details.add(e.getMessage());
        Logs.Log(Logs.LogType.PLAYER, "chat", details);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        List<Object> details = new ArrayList<>();
        details.add(e.getPlayer());
        Logs.Log(Logs.LogType.PLAYER, "respawn", details);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDamaged(EntityDamageEvent e) {
        if (e.isCancelled()) return;
        if (e instanceof EntityDamageByEntityEvent) return;
        if (!(e.getEntity() instanceof Player)) return;
        List<Object> details = new ArrayList<>();
        details.add(e.getEntity());
        details.add(new Logs.DamageCause(e.getCause(), e.getEntity()));
        details.add(e.getFinalDamage());
        Logs.Log(Logs.LogType.PLAYER, "damage", details);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDamagedByEntity(EntityDamageByEntityEvent e) {
        if (e.isCancelled()) return;
        if (e.getEntity() instanceof Player) {
            List<Object> details = new ArrayList<>();
            details.add(e.getEntity());
            details.add(new Logs.DamageCause(e.getCause(), e.getEntity(), e.getDamager()));
            details.add(e.getFinalDamage());
            details.add(e.getDamager());
            Logs.Log(Logs.LogType.PLAYER, "damage", details);
        }
        if (!(e.getDamager() instanceof Player)) return;
        List<Object> details = new ArrayList<>();
        details.add(e.getDamager());
        details.add(new Logs.DamageCause(e.getCause(), e.getEntity(), e.getDamager()));
        details.add(e.getFinalDamage());
        details.add(e.getEntity());
        Logs.Log(Logs.LogType.PLAYER, "attack", details);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getEntity().getKiller() != null) {
            List<Object> details = new ArrayList<>();
            details.add(e.getEntity().getKiller());
            details.add(e.getEntity().getKiller().getInventory().getItemInMainHand());
            details.add(e.getEntity());
            Logs.Log(Logs.LogType.PLAYER, "kill", details);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerDeathEvent e) {
        List<Object> details = new ArrayList<>();
        details.add(e.getEntity());
        if (e.getEntity().getKiller() != null) {
            details.add(e.getEntity().getKiller().getInventory().getItemInMainHand());
            details.add(e.getEntity().getKiller());
        }
        Logs.Log(Logs.LogType.PLAYER, "death", details);
    }
}
