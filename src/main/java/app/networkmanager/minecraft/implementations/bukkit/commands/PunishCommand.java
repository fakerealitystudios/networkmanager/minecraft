package app.networkmanager.minecraft.implementations.bukkit.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.entity.Player;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.commands.BaseCommand;
import app.networkmanager.minecraft.commands.CommandArgument;
import app.networkmanager.minecraft.commands.CommandSender;
import app.networkmanager.minecraft.commands.CommandArgument.ArgType;
import app.networkmanager.minecraft.implementations.bukkit.Implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PunishCommand extends BaseCommand {
    public String getCommand() { return "punish"; }

    public String getHelp() { return "Adds a punishment to the player."; }

    public List<CommandArgument> getArguments() {
        List<CommandArgument> args = new ArrayList<>();
        args.add(new CommandArgument(ArgType.PLAYER, "<player>"));
        args.add(new CommandArgument(ArgType.STRING, "<punishment>"));
        args.add(new CommandArgument(ArgType.STRING, "<reason>"));
        return args;
    };

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.punish");
    }

    // nm punish <player> <punishment> <reason>
    public void onCommand(CommandSender sender, List<String> args)
    {
        if(args.size() < 2) {
            return;
        }
        String player = "";
        String punishment = "";
        String reason = "Unspecified";
        if(args.size() < 3) {
            sender.sendMessage("Missing arguments");
            return;
        } else {
            player = args.get(0);
            args.remove(0);
            punishment = args.get(0);
            args.remove(0);
            reason = StringUtils.join(args, " ");
        }
        List<Player> plys = Implementation.getInstance().getServer().matchPlayer(player);
        Player ply;
        if (plys.size() > 1)
        {
            sender.sendMessage("Too many possible players");
            return;
        } else if (plys.size() == 0) {
            try {
                UUID uuid = UUID.fromString(player);
                ply = Implementation.getInstance().getServer().getPlayer(uuid);
            } catch(IllegalArgumentException e) {
                // Not a UUID neither
                sender.sendMessage("No matching players");
                return;
            }
        } else {
            ply = plys.get(0);
        }
        if (ply == null) {
            sender.sendMessage("Failed to get player");
            return;
        }

        final app.networkmanager.minecraft.types.Player nm_ply = NetworkManager.players.get(ply.getUniqueId());

        Map<String, String> formData = new HashMap<>();
        formData.put("usid", "" + nm_ply.getSessionId());
        formData.put("reason", reason);
        if (sender instanceof Player) {
            final app.networkmanager.minecraft.types.Player nm_admin = NetworkManager.players.get(((Player)sender).getUniqueId());
            formData.put("admin_nmid", "" + nm_admin.getNMId());
            formData.put("admin_usid", "" + nm_admin.getSessionId());
        }

        NetworkManager.api.Put("/member/" + nm_ply.getNMId() + "/punishment/" + punishment, formData);
        sender.sendMessage("Punishment added for " + ply.getDisplayName());
    }
}