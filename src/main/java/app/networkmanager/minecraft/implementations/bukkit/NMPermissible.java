package app.networkmanager.minecraft.implementations.bukkit;

import app.networkmanager.minecraft.types.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class NMPermissible extends PermissibleBase {
    private final Player nm_ply;
    //private final org.bukkit.entity.Player ply;
    //private final JavaPlugin plugin;
    private PermissibleBase oldPermissible = null;

    public NMPermissible(org.bukkit.entity.Player ply, Player nm_ply, JavaPlugin plugin) {
        super(ply);
        this.nm_ply = Objects.requireNonNull(nm_ply, "nm_ply");
        //this.ply = Objects.requireNonNull(ply, "ply");
        //this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    public PermissibleBase getOldPermissible() {
        return this.oldPermissible;
    }

    public void setOldPermissible(PermissibleBase oldPermissible) {
        this.oldPermissible = oldPermissible;
    }

    // Haven't decided if I want to just create a permission for it and say fuck minecraft
    @Override
    public boolean isOp() {
        return super.isOp();
    }

    @Override
    public void setOp(boolean value) {
        super.setOp(value);
    }

    @Override
    public boolean isPermissionSet(String inName) {
        if (nm_ply.hasPermission(inName) == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isPermissionSet(Permission perm) {
        return isPermissionSet(perm.getName());
    }

    @Override
    public boolean hasPermission(String inName) {
        Boolean result = nm_ply.hasPermission(inName);
        if (result == null) {
            if (oldPermissible == null) { return false; }
            return oldPermissible.hasPermission(inName);
        }
        return result.booleanValue();
    }

    @Override
    public boolean hasPermission(Permission perm) {
        return hasPermission(perm.getName());
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
        if (oldPermissible == null) { return null; }
        return oldPermissible.addAttachment(plugin, name, value);
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin) {
        if (oldPermissible == null) { return null; }
        return oldPermissible.addAttachment(plugin);
    }

    @Override
    public void removeAttachment(PermissionAttachment attachment) {
        if (oldPermissible == null) { return; }
        oldPermissible.removeAttachment(attachment);
    }

    @Override
    public void recalculatePermissions() {
        if (oldPermissible == null) { return; }
        oldPermissible.recalculatePermissions();
    }

    @Override
    public void clearPermissions() {
        if (oldPermissible == null) { return; }
        oldPermissible.clearPermissions();
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
        if (oldPermissible == null) { return null; }
        return oldPermissible.addAttachment(plugin, name, value, ticks);
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
        if (oldPermissible == null) { return null; }
        return oldPermissible.addAttachment(plugin, ticks);
    }

    @Override
    public Set<PermissionAttachmentInfo> getEffectivePermissions() {
        Set<PermissionAttachmentInfo> set = new HashSet<>();
        for (String perm : nm_ply.getRealmPermissions())
        {
            if (perm.substring(0, 1) == "!")
                set.add(new PermissionAttachmentInfo(this, perm.substring(1), null, false));
            else
                set.add(new PermissionAttachmentInfo(this, perm, null, true));
        }
        return set;
    }
}
