package app.networkmanager.minecraft.implementations.bukkit.commands;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.commands.BaseCommand;
import app.networkmanager.minecraft.commands.CommandSender;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonObject;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class PanelCommand extends BaseCommand {
    public String getCommand() { return "panel"; }

    public String getHelp() { return "Provides a link to the panel authenticated with the current user."; }

    public boolean needsPlayer()
    {
        return true;
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        // TODO: Check if the account is already linked

        final app.networkmanager.minecraft.types.Player nm_ply = NetworkManager.players.get(sender.getUniqueId());
        if (nm_ply == null || nm_ply.getNMId() == -1) {
            sender.sendMessage("Failed to find NetworkManager account!");
            return;
        }

        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                Map<String, String> formData = new HashMap<>();
                formData.put("type", "minecraft");
                formData.put("token", nm_ply.getUUID().toString());
                formData.put("link", "0");
                JsonObject json = NetworkManager.api.Put("/member/" + nm_ply.getNMId() + "/authenticate", formData).json;
                if (json.get("status").toString().equals("failure")) {
                    sender.sendMessage("Failed to retrieve unique login token!");

                    String url = NetworkManager.settings.get("site");
                    TextComponent urlmsg = new TextComponent(url);
                    urlmsg.setUnderlined(true);
                    urlmsg.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
                    urlmsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(NetworkManager.settings.get("site")).create()));
                    return;
                }

                String token = json.get("token").getAsString();

                String url = NetworkManager.settings.get("site") + "/login.php?authToken="+token;

                TextComponent urlmsg = new TextComponent(token);
                urlmsg.setUnderlined(true);
                urlmsg.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
                urlmsg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(NetworkManager.settings.get("site")).create()));

                TextComponent message = new TextComponent();
                message.addExtra("Use token ");
                message.addExtra(urlmsg);
                message.addExtra(" to connect your NetworkManager accounts. This token will be invalid after 5 minutes.");
                ((org.bukkit.command.CommandSender)sender.getInstance()).spigot().sendMessage(message);
            }
        });
    }
}