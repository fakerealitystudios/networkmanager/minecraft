package app.networkmanager.minecraft.implementations.bukkit.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.commands.BaseCommand;
import app.networkmanager.minecraft.commands.BaseMultiCommand;
import app.networkmanager.minecraft.commands.CommandArgument;
import app.networkmanager.minecraft.commands.CommandArgument.ArgType;
import app.networkmanager.minecraft.implementations.CommandContainer;
import app.networkmanager.minecraft.implementations.bukkit.Implementation;

//https://github.com/chrismin13/AdditionsAPI/blob/0c36f6c078cfa4d30a04739110e76383c7bc7c19/src/main/java/com/chrismin13/additionsapi/commands/AdditionsTab.java
public class TabCompleter implements org.bukkit.command.TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        final List<String> completions = new ArrayList<>();
        if (args.length == 1) {
            StringUtil.copyPartialMatches(args[0], CommandContainer.getCommands().keySet(), completions);
        } else {
            int length = args.length-1;
            BaseCommand subcmd = CommandContainer.getCommands().get(args[0]);
            boolean mutliCmd = false;
            if (subcmd == null) {
                return completions;
            } else if (subcmd instanceof BaseMultiCommand) {
                length--;
                mutliCmd = true;
            }
            if (mutliCmd && args.length == 2) {
                StringUtil.copyPartialMatches(args[1], ((BaseMultiCommand)subcmd).getCommands().keySet(), completions);
            } else {
                if (mutliCmd) {
                    subcmd = ((BaseMultiCommand)subcmd).getCommands().get(args[1]);
                }
                if (length <= subcmd.getArguments().size()) {
                    String str;
                    if (mutliCmd) {
                        str = args[length+1];
                    } else {
                        str = args[length];
                    }
                    CommandArgument cmdarg = subcmd.getArguments().get(length-1);
                    if (cmdarg.arg == ArgType.PLAYER) {
                        Implementation.getInstance().getServer().matchPlayer(str).forEach(pl -> completions.add(pl.getName()));
                    } else if (cmdarg.arg == ArgType.RANK) {
                        StringUtil.copyPartialMatches(str, NetworkManager.ranks.keySet(), completions);
                    } else if (cmdarg.arg == ArgType.REALM) {
                        completions.add(cmdarg.str);
                    } else if (cmdarg.arg == ArgType.ENUM && cmdarg.options != null) {
                        StringUtil.copyPartialMatches(str, cmdarg.options, completions);
                    } else if (str.length() < 1) {
                        if (cmdarg.options != null) {
                            StringUtil.copyPartialMatches(str, cmdarg.options, completions);
                        } else {
                            completions.add(cmdarg.str);
                        }
                    }
                }
            }
        }
        return completions;
    }
}
  