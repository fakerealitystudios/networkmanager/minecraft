package app.networkmanager.minecraft.implementations.bukkit.listeners;

import app.networkmanager.minecraft.ExternalApiRequests;
import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.implementations.bukkit.Implementation;
import app.networkmanager.minecraft.implementations.bukkit.PermissibleInjector;
import app.networkmanager.minecraft.types.Ban;
import app.networkmanager.minecraft.implementations.bukkit.NMPermissible;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent e) {
        int unixTime = (int)(System.currentTimeMillis() / 1000L);
        for (Ban ban : NetworkManager.server.bans) {
            if (ban.uuid.equals(e.getUniqueId()) && ban.unbanTime > unixTime) {
                e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ban.reason);
                return;
            }
        }
        try {
            final app.networkmanager.minecraft.types.Player nm_ply = new app.networkmanager.minecraft.types.Player(e.getUniqueId(), e.getName());
            nm_ply.setAddress(e.getAddress().getHostAddress());
            NetworkManager.players.put(nm_ply.getUUID(), nm_ply);
        } catch (Throwable t) {
            NetworkManager.sentry.sendException(t);
            t.printStackTrace();
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "Failed to load NetworkManager data");
            return;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(PlayerLoginEvent e) {
        final Player ply = e.getPlayer();
        final app.networkmanager.minecraft.types.Player nm_ply = NetworkManager.players.get(ply.getUniqueId());
        if (nm_ply.getNMId() == -1)
        {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Failed to load NetworkManager data!");
            return;
        }

        try {
            // Make a new permissible for the user
            NMPermissible permissible = new NMPermissible(ply, nm_ply, Implementation.getInstance());

            // Inject into the player
            PermissibleInjector.inject(ply, permissible);

        } catch (Throwable t) {
            NetworkManager.sentry.sendException(t);
            NetworkManager.LogError("Exception thrown when setting up permissions for " +
                    ply.getUniqueId() + " - " + ply.getName() + " - denying login.");
            t.printStackTrace();

            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Error creating permissible!");
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        final Player ply = e.getPlayer();

        final app.networkmanager.minecraft.types.Player nm_ply = NetworkManager.players.get(ply.getUniqueId());
        /*if (nm_ply.getJustCreated()) {
            ply.sendMessage("A new NetworkManager account has been created! If you already have an account, please use '/nm link'");
        }*/
        
        if (NetworkManager.config.bungeecord == false) {
            nm_ply.setAddress(ply.getAddress().getAddress().getHostAddress());
        } else {// We'll assume that, if offline, we're running with BungeeCord(if not, welp, oh well. Maybe don't run a cracked server?)
            NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
                @Override
                public void run() {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("IP");
                    ply.sendPluginMessage(Implementation.getInstance(), "BungeeCord", out.toByteArray());
                }
            });
        }

        if (ExternalApiRequests.updateVersion && ply.hasPermission("nm.version")) {
            ply.sendMessage("[NM] There is an update available for NetworkManager! Please download it from your web panel.");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLeave(PlayerQuitEvent e) {
        final Player ply = e.getPlayer();

        org.bukkit.Bukkit.getScheduler().runTaskLaterAsynchronously(Implementation.getInstance(), () -> {
            // Remove the custom permissible
            try {
                PermissibleInjector.unInject(ply, true);
            } catch (Exception t) {
                NetworkManager.sentry.sendException(t);
                t.printStackTrace();
            }
            
            if (NetworkManager.players.containsKey(ply.getUniqueId())) {
                NetworkManager.players.get(ply.getUniqueId()).unregisterPlayer();
                NetworkManager.players.remove(ply.getUniqueId());
            }
        }, 1L);
    }
}
