package app.networkmanager.minecraft.implementations.bukkit;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.Realms;
import app.networkmanager.minecraft.types.Player;
import app.networkmanager.minecraft.types.Rank;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;

public class VaultPermissions extends Permission {

    @Override
    public String getName() {
        return "NetworkManager";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean hasSuperPermsCompat() {
        return true;
    }

    // Should lookup by name, uuid, whatever else
    protected Player GetNMPlayer(String player) {
        List<org.bukkit.entity.Player> plys = Implementation.getInstance().getServer().matchPlayer(player);
        if (plys.size() == 1) {
            return NetworkManager.players.get(plys.get(0).getUniqueId());
        }
        return null;
    }

    protected Player GetNMPlayer(OfflinePlayer player) {
        return NetworkManager.players.get(player.getUniqueId());
    }

    public boolean playerHas(String world, Player nm_ply, String permission) {
        if (nm_ply != null) {
            if (world != null) {
                Boolean result = nm_ply.hasPermission(permission, nm_ply.getDefaultRealm() + "." + world);
                if (result != null) {
                    return result.booleanValue();
                }
                return false;
            }
            Boolean result = nm_ply.hasPermission(permission);
            if (result != null) {
                return result.booleanValue();
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean playerHas(String world, String player, String permission) {
        return playerHas(world, GetNMPlayer(player), permission);
    }

    @Override
    public boolean playerHas(String world, OfflinePlayer player, String permission) {
        return playerHas(world, GetNMPlayer(player), permission);
    }

    // Yeah, uhh, you can't influence NetworkManager, sorry
    @Override
    public boolean playerAdd(String world, String player, String permission) {
        return false;
    }

    // Yeah, uhh, you can't influence NetworkManager, sorry
    /*@Override
    public boolean playerAddTransient(Player player, String permission) {
        return false;
    }*/

    // Yeah, uhh, you can't influence NetworkManager, sorry
    @Override
    public boolean playerRemove(String world, String player, String permission) {
        return false;
    }

    /*@Override
    public boolean playerRemoveTransient(Player player, String permission) {
        return false;
    }*/

    @Override
    public boolean groupHas(String world, String group, String permission) {
        final Rank rank = NetworkManager.ranks.get(group);
        if (world != null) {
            Boolean result = rank.hasPermission(permission, rank.getDefaultRealm() + "." + world);
            if (result != null) {
                return result.booleanValue();
            }
            return false;
        }
        Boolean result = rank.hasPermission(permission);
        if (result != null) {
            return result.booleanValue();
        }
        return false;
    }

    // Yeah, uhh, you can't influence NetworkManager, sorry
    @Override
    public boolean groupAdd(String world, String group, String permission) {
        return false;
    }

    // Yeah, uhh, you can't influence NetworkManager, sorry
    @Override
    public boolean groupRemove(String world, String group, String permission) {
        return false;
    }

    public boolean playerInGroup(String world, Player nm_ply, String group) {
        if (nm_ply != null) {
            for(Player.PlayerRank rank : nm_ply.getRankData())
            {
                if (rank.rank.getRank().equals(group)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean playerInGroup(String world, String player, String group) {
        return playerInGroup(world, GetNMPlayer(player), group);
    }

    @Override
    public boolean playerInGroup(String world, OfflinePlayer player, String group) {
        return playerInGroup(world, GetNMPlayer(player), group);
    }

    // Yeah, uhh, you can't influence NetworkManager, sorry
    @Override
    public boolean playerAddGroup(String s, String s1, String s2) {
        return false;
    }

    // Yeah, uhh, you can't influence NetworkManager, sorry
    @Override
    public boolean playerRemoveGroup(String s, String s1, String s2) {
        return false;
    }

    public String[] getPlayerGroups(String world, Player nm_ply) {
        if (nm_ply != null) {
            List<String> ranks = new ArrayList<>();
            for(Rank rank : nm_ply.getSubranks())
            {
                ranks.add(rank.getRank());
            }
            return ranks.toArray(new String[ranks.size()]);
        }
        return new String[0];
    }

    @Override
    public String[] getPlayerGroups(String world, String player) {
        return getPlayerGroups(world, GetNMPlayer(player));
    }

    @Override
    public String[] getPlayerGroups(String world, OfflinePlayer player) {
        return getPlayerGroups(world, GetNMPlayer(player));
    }

    public String getPrimaryGroup(String world, Player nm_ply) {
        if (nm_ply != null) {
            return nm_ply.getRank(Realms.realm + "." + world).getRank();
        }
        return "user";
    }

    @Override
    public String getPrimaryGroup(String world, String player) {
        return getPrimaryGroup(world, GetNMPlayer(player));
    }

    @Override
    public String getPrimaryGroup(String world, OfflinePlayer player) {
        return getPrimaryGroup(world, GetNMPlayer(player));
    }

    @Override
    public String[] getGroups() {
        return NetworkManager.ranks.keySet().toArray(new String[NetworkManager.ranks.size()]);
    }

    @Override
    public boolean hasGroupSupport() {
        return true;
    }
}
