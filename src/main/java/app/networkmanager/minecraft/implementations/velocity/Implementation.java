package app.networkmanager.minecraft.implementations.velocity;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.NetworkManager.LogLevel;
import app.networkmanager.minecraft.implementations.NetworkManagerPlugin;
import app.networkmanager.minecraft.implementations.velocity.listeners.PlayerListener;
import app.networkmanager.minecraft.types.Player;

import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.scheduler.Scheduler.TaskBuilder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyReloadEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;

import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;

import com.google.inject.Inject;
import org.slf4j.Logger;
import net.kyori.text.TextComponent;

@Plugin(id = "networkmanager", name = "Network Manager", url = "https://www.networkmanager.io/", authors = {"Montana Tuska"},
    version = "1.0.0", description = "Connects to a NetworkManager API for administration, logging, and more.")
public class Implementation implements NetworkManagerPlugin {
    private static Implementation instance;
    private final ProxyServer server;
    private final Logger logger;

    @Inject
    public Implementation(ProxyServer server, Logger logger) {
        instance = this;
        this.server = server;
        this.logger = logger;

        logger.info("NetworkManager initialized");
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {
        new NetworkManager(this, NetworkManager.PluginType.VELOCITY);
        server.getEventManager().register(this, new PlayerListener());
    }

    @Subscribe
    public void onProxyReload(ProxyReloadEvent event) {
        
    }

    @Subscribe
    public void onProxyShutdown(ProxyShutdownEvent event) {

    }

    public static Implementation getInstance() {
        return instance;
    }

    @Override
    public Object getPluginServer() {
        return server;
    }

    @Override
    public void Reload() {

    }

    @Override
    public String getIp() {
        return server.getBoundAddress().getAddress().getHostAddress();
    }

    @Override
    public int getPort() {
        return server.getBoundAddress().getPort();
    }

    @Override
    public boolean isPrimaryThread() {
        return false;
    }

    @Override
    public void scheduleRepeatingAsyncTask(Runnable runnable, Long delay, Long period) {
        TaskBuilder task = server.getScheduler().buildTask(this, runnable);
        if (delay < period)
            task.delay(delay/20L, TimeUnit.SECONDS);
        task.repeat(period/20L, TimeUnit.SECONDS);
    }

    @Override
    public void scheduleAsyncTask(Runnable runnable, Long delay) {
        server.getScheduler().buildTask(this, runnable).delay(delay/20L, TimeUnit.SECONDS);
    }

    @Override
    public void runAsyncTask(Runnable runnable) {
        server.getScheduler().buildTask(this, runnable).schedule();
    }

    @Override
    public void scheduleRepeatingSyncTask(Runnable runnable, Long delay, Long period) {
        TaskBuilder task = server.getScheduler().buildTask(this, runnable);
        if (delay < period)
            task.delay(delay/20L, TimeUnit.SECONDS);
        task.repeat(period/20L, TimeUnit.SECONDS);
    }

    @Override
    public void scheduleSyncTask(Runnable runnable, Long delay) {
        server.getScheduler().buildTask(this, runnable).delay(delay/20L, TimeUnit.SECONDS);
    }

    @Override
    public void runSyncTask(Runnable runnable) {
        server.getScheduler().buildTask(this, runnable).schedule();
    }

    @Override
    public void Log(String msg, LogLevel level) {
        if (level == LogLevel.DEBUG)
            logger.debug(msg);
        else if (level == LogLevel.INFO)
            logger.info(msg);
        else if (level == LogLevel.WARNING)
            logger.warn(msg);
        else if (level == LogLevel.ERROR)
            logger.error(msg);
    }

    @Override
    public void dispatchCommand(String cmd) {

    }

    @Override
    public void broadcastMessage(String msg) {
        
    }

    @Override
    public List<Player> matchPlayer(String player) {
        return new ArrayList();
    }

    @Override
    public boolean isOnline(UUID uuid) {
        return server.getPlayer(uuid).isPresent();
    }

    @Override
    public void kick(UUID uuid, String reason) {
        try {
            server.getPlayer(uuid).get().disconnect(TextComponent.of(reason));
        } catch(NoSuchElementException e) {}
    }
}