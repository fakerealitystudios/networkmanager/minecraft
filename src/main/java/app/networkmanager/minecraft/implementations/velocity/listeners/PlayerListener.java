package app.networkmanager.minecraft.implementations.velocity.listeners;

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.connection.PostLoginEvent;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.types.Player;
import net.kyori.text.TextComponent;

public class PlayerListener {
    @Subscribe
    public void onPostLogin(PostLoginEvent e) {
        final Player nm_ply = new Player(e.getPlayer().getUniqueId(), e.getPlayer().getUsername());
        nm_ply.setAddress(e.getPlayer().getRemoteAddress().getAddress().getHostAddress());
        if (nm_ply.getNMId() == -1) {
            e.getPlayer().disconnect(TextComponent.of("BungeeCord: Failed to load NetworkManager data!"));
            return;
        }
        NetworkManager.players.put(e.getPlayer().getUniqueId(), nm_ply);
    }

    @Subscribe
    public void onDisconnect(DisconnectEvent e) {
        NetworkManager.players.get(e.getPlayer().getUniqueId()).unregisterPlayer();
        NetworkManager.players.remove(e.getPlayer().getUniqueId());
    }
}