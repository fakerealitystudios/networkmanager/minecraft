package app.networkmanager.minecraft.implementations;

import java.util.UUID;
import java.util.List;

import app.networkmanager.minecraft.NetworkManager.LogLevel;
import app.networkmanager.minecraft.types.Player;

public interface NetworkManagerPlugin {
    public Object getPluginServer();
    public void Reload();
    public String getIp();
    public int getPort();
    public boolean isPrimaryThread();
    default public void scheduleRepeatingAsyncTask(Runnable runnable, int delay, int period) {
        scheduleRepeatingAsyncTask(runnable, delay * 20L, period * 20L);
    }
    public void scheduleRepeatingAsyncTask(Runnable runnable, Long delay, Long period);
    default public void scheduleAsyncTask(Runnable runnable, int delay) {
        scheduleAsyncTask(runnable, delay * 20L);
    }
    public void scheduleAsyncTask(Runnable runnable, Long delay);
    default public void scheduleRepeatingSyncTask(Runnable runnable, int delay, int period) {
        scheduleRepeatingSyncTask(runnable, delay * 20L, period * 20L);
    }
    public void scheduleRepeatingSyncTask(Runnable runnable, Long delay, Long period);
    public void runAsyncTask(Runnable runnable);
    default public void scheduleSyncTask(Runnable runnable, int delay) {
        scheduleSyncTask(runnable, delay * 20L);
    }
    public void scheduleSyncTask(Runnable runnable, Long delay);
    public void runSyncTask(Runnable runnable);
    public void Log(String msg, LogLevel level);
    public void dispatchCommand(String cmd);
    public void broadcastMessage(String msg);
    public List<Player> matchPlayer(String player);
    public boolean isOnline(UUID uuid);
    public void kick(UUID uuid, String reason);
}
