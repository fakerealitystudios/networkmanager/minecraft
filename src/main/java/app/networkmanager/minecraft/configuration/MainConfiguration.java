package app.networkmanager.minecraft.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainConfiguration extends BaseConfiguration {

    @Override
    public int getConfigVersion() {
        return 3;
    }

    public Boolean bungeecord = false; // Enable to use the plugin messaging to share a single web connection.
    public Boolean debug = false;
    public Boolean debugHTTP = false;
    public Api api = new Api();
    public External_Api external_api = new External_Api();
    public List<String> ignorePermissions = new ArrayList<>();

    public static class Api {
        public String endpoint = "https://demo.networkmanager.io";
        public String server_name = "minecraft";
        public Integer server_action_poll = 15; // Only uses polling when sockets not available
    }

    public static class External_Api {
        public String endpoint = "https://api.networkmanager.center";
        public Boolean statistics = true;
    }

    public static MainConfiguration initialize() {
        File folder = new File("plugins/NetworkManager");
        if (!folder.exists())
            folder.mkdirs();
        return BaseConfiguration.readYaml(new File("plugins/NetworkManager/config.yml"), MainConfiguration.class);
    }
}
