package app.networkmanager.minecraft.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PermissionsCache extends BaseConfiguration {

    @Override
    public int getConfigVersion() {
        return 1;
    }

    public List<String> permissions = new ArrayList<>();

    public static PermissionsCache initialize() {
        File folder = new File("plugins/NetworkManager");
        if (!folder.exists())
            folder.mkdirs();
        return BaseConfiguration.readYaml(new File("plugins/NetworkManager/permission_cache.yml"), PermissionsCache.class);
    }
}
