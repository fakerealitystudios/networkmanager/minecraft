package app.networkmanager.minecraft.configuration;

import java.io.File;

public class ServerConfiguration extends BaseConfiguration {

    @Override
    public int getConfigVersion() {
        return 2;
    }

    public Integer server_id = -1;
    public String secret = "";
    public String settings = "[]";

    public static ServerConfiguration initialize() {
        File folder = new File("plugins/NetworkManager");
        if (!folder.exists())
            folder.mkdirs();
        return BaseConfiguration.readYaml(new File("plugins/NetworkManager/server.yml"), ServerConfiguration.class);
    }
}
