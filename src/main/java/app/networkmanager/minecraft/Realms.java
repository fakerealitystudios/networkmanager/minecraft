package app.networkmanager.minecraft;

import java.util.HashMap;
import java.util.regex.Pattern;

public class Realms {
    public static final String delimiter = ".";
    public static String realm = "*.minecraft";
    private static HashMap<String, Realms> realmMap = new HashMap<>();
    private String[] realmSplit;

    private Realms(String fullRealm) {
        realmSplit = fullRealm.split(Pattern.quote(delimiter));
    }

    public static Realms getRealm(String fullRealm) {
        if (realmMap.containsKey(fullRealm)) {
            return realmMap.get(fullRealm);
        }
        realmMap.put(fullRealm, new Realms(fullRealm));
        return realmMap.get(fullRealm);
    }

    public String toString() {
        return String.join(delimiter, realmSplit);
    }

    public String[] getParts() {
        return realmSplit;
    }
}
