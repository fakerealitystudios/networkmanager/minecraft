package app.networkmanager.minecraft;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

import app.networkmanager.minecraft.HTTP;

public class API extends HTTP {

    public API() {
        this.endpoint = NetworkManager.config.api.endpoint + "/api/index.php?";
    }

    @Override
    protected HTTP.Response Process(HTTP.Response resp) {
        if (resp != null) {
            if (resp.json == null) {
                NetworkManager.LogHTTP("API Error: No response");
                resp.json = new JsonObject();
                resp.json.addProperty("status", "failure");
                resp.json.addProperty("error", "No response!");
                resp.json.addProperty("errorcode", "999");
            } else {
                NetworkManager.LogHTTP("API Response: " + resp.content);
                if (resp.json.get("status").getAsString().equalsIgnoreCase("failure")) {
                    NetworkManager.LogError("API Error " + resp.json.get("errorcode").getAsString() + ": " + resp.json.get("error").getAsString());
                }
            }
        }

        return resp;
    }

    @Override
    protected Map<String, String> Headers() {
        Map<String, String> map = new HashMap<>();
        if (NetworkManager.config_server.server_id != -1) {
            map.put("Server-Id", String.valueOf(NetworkManager.config_server.server_id));
        }
        if (!NetworkManager.config_server.secret.isEmpty()) {
            map.put("Authorization", NetworkManager.config_server.secret);
        }
        map.put("Accept", "application/json");
        return map;
    }
}