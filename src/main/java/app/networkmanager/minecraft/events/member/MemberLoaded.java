package app.networkmanager.minecraft.events.member;

import app.networkmanager.minecraft.events.Event;
import app.networkmanager.minecraft.types.Player;

public class MemberLoaded extends Event {
    private final Player nm_ply;

    public MemberLoaded(Player nm_ply) {
        this.nm_ply = nm_ply;
    }

    public Player getNMPlayer() {
        return nm_ply;
    }
}
