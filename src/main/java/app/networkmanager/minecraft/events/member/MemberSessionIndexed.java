package app.networkmanager.minecraft.events.member;

import app.networkmanager.minecraft.events.Event;
import app.networkmanager.minecraft.types.Player;

public class MemberSessionIndexed extends Event {
    private final Player nm_ply;
    private final int sessionid;

    public MemberSessionIndexed(Player nm_ply, int sessionid) {
        this.nm_ply = nm_ply;
        this.sessionid = sessionid;
    }

    public Player getNMPlayer() {
        return nm_ply;
    }
    public int getSessionId() {
        return sessionid;
    }
}
