package app.networkmanager.minecraft.events.member;

import app.networkmanager.minecraft.events.Event;

public class MemberIndexed extends Event {
    private final int id;

    public MemberIndexed(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
