package app.networkmanager.minecraft.events.server;

import app.networkmanager.minecraft.events.Event;

public class ServerSessionIndexed extends Event {
    private final int sessionid;

    public ServerSessionIndexed(int sessionid) {
        this.sessionid = sessionid;
    }

    public int getSessionId() {
        return sessionid;
    }
}
