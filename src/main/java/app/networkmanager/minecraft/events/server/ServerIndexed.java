package app.networkmanager.minecraft.events.server;

import app.networkmanager.minecraft.events.Event;

public class ServerIndexed extends Event {
    private final int id;

    public ServerIndexed(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
