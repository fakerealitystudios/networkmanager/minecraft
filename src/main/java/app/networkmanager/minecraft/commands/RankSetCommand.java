package app.networkmanager.minecraft.commands;

import app.networkmanager.minecraft.commands.CommandArgument.ArgType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RankSetCommand extends BaseCommand {
    public String getCommand() { return "set"; }

    public String getHelp() { return "Set a player's rank(on this server)"; }

    public List<CommandArgument> getArguments() {
        List<CommandArgument> args = new ArrayList<>();
        args.add(new CommandArgument(ArgType.PLAYER, "<player>"));
        args.add(new CommandArgument(ArgType.RANK, "<rank>"));
        args.add(new CommandArgument(ArgType.REALM, "[realm]"));
        return args;
    };

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.help");
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        sender.sendMessage("Rank Set");
    }
}
