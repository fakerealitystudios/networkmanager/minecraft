package app.networkmanager.minecraft.commands;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class BaseMultiCommand extends BaseCommand {
    private Map<String, BaseCommand> subCommands = new TreeMap<>();

    protected void addComand(BaseCommand s)
    {
        subCommands.put(s.getCommand(), s);
    }

    public Map<String, BaseCommand> getCommands()
    {
        return subCommands;
    }

    public String getHelp() { return ""; }

    public void onCommand(CommandSender sender, List<String> args)
    {
        if (args.size() < 1) {
            // perform help
        } else {
            String cmd = args.get(0).toLowerCase();
            if (subCommands.containsKey(cmd)) {
                args.remove(0);
                subCommands.get(cmd).runCommand(sender, args);
            } else {
                // command not found! Boo!
            }
        }
    }
}