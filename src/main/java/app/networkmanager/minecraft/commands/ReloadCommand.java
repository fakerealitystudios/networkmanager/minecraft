package app.networkmanager.minecraft.commands;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.commands.CommandArgument.ArgType;
import app.networkmanager.minecraft.configuration.MainConfiguration;
import app.networkmanager.minecraft.configuration.ServerConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReloadCommand extends BaseCommand {
    public String getCommand() { return "reload"; }

    public String getHelp() { return "Reload NetworkManager config"; }

    public List<CommandArgument> getArguments() {
        List<String> options = new ArrayList<>();
        options.add("ranks");
        List<CommandArgument> args = new ArrayList<>();
        args.add(new CommandArgument(ArgType.ENUM, "[options]", options));
        return args;
    };

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.reload");
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        if (args.size() > 0) {
            if (args.get(0) == "ranks") {
                NetworkManager.instance.loadRanks();
            }
        } else {
            NetworkManager.instance.onDisable();

            NetworkManager.config = MainConfiguration.initialize();
            NetworkManager.config_server = ServerConfiguration.initialize();
            sender.sendMessage("Configuration reloaded!");

            NetworkManager.instance.onEnable();
            NetworkManager.plugin.Reload();
        }
    }
}
