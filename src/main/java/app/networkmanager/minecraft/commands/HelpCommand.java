package app.networkmanager.minecraft.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.implementations.ChatColor;
import app.networkmanager.minecraft.implementations.CommandContainer;

public class HelpCommand extends BaseCommand {
    public String getCommand() { return "help"; }

    public String getHelp() { return "Used to display a list of available commands"; }

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.help");
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        if (CommandContainer.getCommands().size() <= 1) {
            sender.sendMessage("No commands have been registered!");
            return;
        }

        int page;

        if(args.size() < 1)
        {
            page = 1;
        }
        else
        {
            try
            {
                page = Integer.parseInt(args.get(0));
            }
            catch (Exception e)
            {
                NetworkManager.sentry.sendException(e);
                sender.sendMessage("Invalid help page!");
                return;
            }
        }

        int pages = CommandContainer.getCommands().size() / 5 + 1;
        if (page < 1 || page > pages)
        {
            sender.sendMessage("We seem to be missing that page!");
            return;
        }

        sender.sendMessage(ChatColor.GOLD + "---------- " + ChatColor.DARK_PURPLE + "NetworkManager Help " + ChatColor.GOLD + "(" + ChatColor.DARK_PURPLE + page + ChatColor.GOLD + "/" + ChatColor.DARK_PURPLE + pages + ChatColor.GOLD + ") ---------");

        int min = (page - 1) * 5;
        int max = page * 5;
        int i = -1;
        for(Map.Entry<String, BaseCommand> entry : CommandContainer.getCommands().entrySet())
        {
            i++;
            if (i < min) { continue; }
            if (i >= max) { break; }
            BaseCommand hcmd = entry.getValue();
            if (hcmd == null || hcmd.isSecret()) { i--; continue; }
            StringBuilder sb = new StringBuilder(ChatColor.GOLD + "- " + ChatColor.DARK_PURPLE + hcmd.getCommand());
            sb.append(ChatColor.GOLD + " - " + ChatColor.GREEN);
            sb.append(hcmd.getCompiledHelp());
            sender.sendMessage(sb.toString());
        }

        sender.sendMessage(ChatColor.GOLD + "----------------------------------------");

        /*if(args.length <= 1)
        {
            Bukkit.dispatchCommand(sender, "lm help 1");
        }
        else
        {
            int id;

            try
            {
                id = Integer.parseInt(args[1]);
            }
            catch(Exception e)
            {
                NetworkManager.sentry.sendException(e);
                sender.sendMessage(MessageUtils.getMessage("Invalid help menu ID!"));
                return;
            }

            if(pages < id)
            {
                sender.sendMessage(MessageUtils.getMessage("Invalid help menu ID!"));
                return;
            }



            sender.sendMessage(ChatColor.GOLD + "---------- " + ChatColor.DARK_PURPLE + "LaunchMe Help " + ChatColor.GOLD + "(" + ChatColor.DARK_PURPLE + id + ChatColor.GOLD + "/" + ChatColor.DARK_PURPLE + pages + ChatColor.GOLD + ") ---------");

            for(int i = (id * 5 - 5); i < id * 5; i++)
            {
                if(!(cmds.size() > i)) break;

                StringBuilder sb = new StringBuilder(ChatColor.GOLD + "- " + ChatColor.DARK_PURPLE + cmds.get(i));
                sb.append(ChatColor.GOLD + " - " + ChatColor.GREEN);
                sb.append(descs.get(i));
                sender.sendMessage(sb.toString());
            }

            sender.sendMessage(ChatColor.GOLD + "----------------------------------------");
        }*/
    }
}
