package app.networkmanager.minecraft.commands;

import java.util.List;

public class CommandArgument
{
    public enum ArgType {
        PLAYER,
        RANK,
        REALM,
        ENUM,
        STRING,
    }

    public ArgType arg;
    public String str;
    public List<String> options;

    public CommandArgument(ArgType arg, String str) {
        this.arg = arg;
        this.str = str;
    }

    public CommandArgument(ArgType arg, String str, List<String> options) {
        this.arg = arg;
        this.str = str;
        this.options = options;
    }
}