package app.networkmanager.minecraft.commands;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseCommand
{
    abstract public String getCommand();

    abstract public String getHelp();

    public String getCompiledHelp()
    {
        return getHelp();
    }

    public List<String> getPermissions() { return new ArrayList<>(); };
    
    public List<CommandArgument> getArguments() { return new ArrayList<>(); };

    public boolean isSecret() { return false; }

    public boolean needsPlayer() { return false; }

    abstract public void onCommand(CommandSender sender, List<String> args);

    public boolean runCommand(CommandSender sender, List<String> args)
    {
        for (String fullPerm : getPermissions())
        {
            if(!sender.hasPermission(fullPerm))
            {
                sender.sendMessage("You do not have permission to perform this command!");
                return false;
            }
        }

        if(needsPlayer() && !sender.isPlayer())
        {
            sender.sendMessage("This command can't be run by console!");
            return false;
        }

        onCommand(sender, args);

        return true;
    }

}
