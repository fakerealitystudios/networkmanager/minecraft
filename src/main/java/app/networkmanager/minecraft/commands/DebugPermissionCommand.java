package app.networkmanager.minecraft.commands;

import java.util.List;

public class DebugPermissionCommand extends BaseCommand {
    public String getCommand() { return "permission"; }

    public String getHelp() { return "Test a specific permission."; }

    public boolean needsPlayer()
    {
        return true;
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        if (args.size() < 1) {
            sender.sendMessage("Requires a permission to test!");
            return;
        }
        sender.sendMessage("Has Permission " + args.get(0) + ": " + sender.hasPermission(args.get(0)));
    }
}
