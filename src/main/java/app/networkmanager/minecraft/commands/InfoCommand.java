package app.networkmanager.minecraft.commands;

import java.util.List;

public class InfoCommand extends BaseCommand {
    public String getCommand() { return "info"; }

    public String getHelp() { return "Used to display a list of available commands"; }

    /*public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.help");
    }*/

    public void onCommand(CommandSender sender, List<String> args)
    {
        sender.sendMessage("MadkillerMax spent a lot of time making this and he feels special.");
    }
}
