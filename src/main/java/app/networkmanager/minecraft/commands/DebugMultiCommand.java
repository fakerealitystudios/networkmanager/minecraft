package app.networkmanager.minecraft.commands;

public class DebugMultiCommand extends BaseMultiCommand {
    {
        addComand(new DebugRanksCommand());
        addComand(new DebugPermissionCommand());
        addComand(new DebugPermissionsCommand());
        addComand(new DebugRanklistCommand());
    }

    public String getCommand() { return "debug"; }
}
