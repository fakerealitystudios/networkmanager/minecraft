package app.networkmanager.minecraft.commands;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.types.Rank;

import java.util.List;

public class DebugRanksCommand extends BaseCommand {
    public String getCommand() { return "ranks"; }

    public String getHelp() { return "See a player's ranks"; }

    public boolean needsPlayer()
    {
        return true;
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        final app.networkmanager.minecraft.types.Player nm_ply = sender.getNMPlayer();
        if (nm_ply == null || nm_ply.getNMId() == -1) {
            sender.sendMessage("Failed to find NetworkManager account!");
            return;
        }
        sender.sendMessage("Rank: " + nm_ply.getRank().getDisplayName() + " & subranks:");
        
        for (Rank rank : nm_ply.getSubranks()) {
            sender.sendMessage("\t"+rank.getDisplayName());
        }
    }
}
