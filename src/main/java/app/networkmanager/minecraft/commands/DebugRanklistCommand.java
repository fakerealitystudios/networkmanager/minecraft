package app.networkmanager.minecraft.commands;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.implementations.ChatColor;
import app.networkmanager.minecraft.types.Rank;

import java.util.List;

public class DebugRanklistCommand extends BaseCommand {
    public String getCommand() { return "ranklist"; }

    public String getHelp() { return "List Ranks"; }

    public boolean needsPlayer()
    {
        return true;
    }

    public void onCommand(CommandSender sender, List<String> args)
    {
        sender.sendMessage("------" + ChatColor.DARK_PURPLE + NetworkManager.ranks.size() + " ranks" + ChatColor.WHITE + "------");
        for (Rank rank : NetworkManager.ranks.values()) {
            if (rank.getInherits() != null)
                sender.sendMessage(rank.getDisplayName() + ": " + rank.getInherits().getDisplayName());
            else
            sender.sendMessage(rank.getDisplayName() + ": " + "null");
        }
        //sender.sendMessage();
    }
}
