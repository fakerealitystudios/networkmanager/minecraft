package app.networkmanager.minecraft.commands;

import org.apache.commons.lang.StringUtils;

import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.commands.CommandArgument.ArgType;
import app.networkmanager.minecraft.implementations.bukkit.Implementation;
import app.networkmanager.minecraft.types.Ban;
import app.networkmanager.minecraft.types.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RealmBanCommand extends BaseCommand {
    public String getCommand() { return "realmban"; }

    public String getHelp() { return "Performs a ban on a player."; }

    public List<CommandArgument> getArguments() {
        List<CommandArgument> args = new ArrayList<>();
        args.add(new CommandArgument(ArgType.PLAYER, "<player>"));
        args.add(new CommandArgument(ArgType.STRING, "<realm>"));
        args.add(new CommandArgument(ArgType.STRING, "<time>"));
        args.add(new CommandArgument(ArgType.STRING, "[reason]]"));
        return args;
    };

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.ban");
    }

    // nm realmban <player> <realm> <time> [reason]
    public void onCommand(CommandSender sender, List<String> args)
    {
        if(args.size() < 2) {
            return;
        }
        String player = "";
        String realm = "";
        String time = "";
        String reason = "Unspecified";
        if(args.size() == 3) {
            player = args.get(0);
            realm = args.get(1);
            time = args.get(2);
        } else {
            player = args.get(0);
            args.remove(0);
            realm = args.get(0);
            args.remove(0);
            time = args.get(0);
            args.remove(0);
            reason = StringUtils.join(args, " ");
            //reason = args.get("the rest");
        }
        List<Player> plys = NetworkManager.getNMPlugin().matchPlayer(player);
        Player nm_ply;
        if (plys.size() > 1)
        {
            sender.sendMessage("Too many possible players");
            return;
        } else if (plys.size() == 0) {
            try {
                UUID uuid = UUID.fromString(player);
                nm_ply = NetworkManager.players.get(uuid);
            } catch(IllegalArgumentException e) {
                NetworkManager.sentry.sendException(e);
                // Not a UUID neither
                sender.sendMessage("No matching players");
                return;
            }
        } else {
            nm_ply = plys.get(0);
        }
        if (nm_ply == null) {
            sender.sendMessage("Failed to get player");
            return;
        }
        int length = Integer.parseInt(time) * 60;

        Map<String, String> formData = new HashMap<>();
        formData.put("usid", Integer.toString(nm_ply.getSessionId()));
        formData.put("length", Integer.toString(length));
        formData.put("reason", reason);
        if (sender.isPlayer()) {
            final Player nm_admin = sender.getNMPlayer();
            formData.put("admin_nmid", Integer.toString(nm_admin.getNMId()));
            formData.put("admin_usid", Integer.toString(nm_admin.getSessionId()));
        }
        Map<String, String> headerData = new HashMap<>();
        headerData.put("Realm", realm);

        NetworkManager.api.Put("/member/" + nm_ply.getNMId() + "/ban", formData, headerData);

        int unixTime = (int)(System.currentTimeMillis() / 1000L);

        Ban ban = new Ban(nm_ply.getNMId(), unixTime + length, nm_ply.getUUID(), reason);
        NetworkManager.server.bans.add(ban);
        NetworkManager.getNMPlugin().broadcastMessage("Player " + nm_ply.getDisplayName() + " has been banned for " + time + " years! [" + reason + "]");
        NetworkManager.getNMPlugin().kick(nm_ply.getUUID(), "[NM] " + reason);
    }
}