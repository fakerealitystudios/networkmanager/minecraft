package app.networkmanager.minecraft.commands;

import java.util.UUID;

import app.networkmanager.minecraft.types.Player;

public abstract class CommandSender {
    public abstract Object getInstance();
    public abstract void sendMessage(String msg);
	public abstract boolean hasPermission(String fullPerm);
	public abstract boolean isPlayer();
	public abstract Player getNMPlayer();
	public abstract UUID getUniqueId();
}