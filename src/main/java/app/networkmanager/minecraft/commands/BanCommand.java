package app.networkmanager.minecraft.commands;

import app.networkmanager.minecraft.Realms;
import app.networkmanager.minecraft.commands.CommandArgument.ArgType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BanCommand extends RealmBanCommand {
    public String getCommand() { return "ban"; }

    public String getHelp() { return "Performs a ban on a player."; }

    public List<CommandArgument> getArguments() {
        List<CommandArgument> args = new ArrayList<>();
        args.add(new CommandArgument(ArgType.PLAYER, "<player>"));
        args.add(new CommandArgument(ArgType.STRING, "<time>"));
        args.add(new CommandArgument(ArgType.STRING, "[reason]"));
        return args;
    };

    public List<String> getPermissions()
    {
        return Arrays.asList("nm.commands.ban");
    }

    // RealmBan without the need of a realm
    public void onCommand(CommandSender sender, List<String> args)
    {
        List<String> newargs = new ArrayList<>();
        newargs.add(args.get(0)); // Player is first argument
        args.remove(0);
        newargs.add(Realms.realm);
        newargs.addAll(args);
        super.onCommand(sender, newargs);
    }
}