package app.networkmanager.minecraft;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Logs {
    private Logs() {}

    public enum LogType {
        SERVER,
        PLAYER;
    }

    public enum DetailType {
        NMID,
		USID,
		TOKENID,
        ENTITY,
        NPC, // Most of minecraft doesn't really have NPC's, we'll leave it here just in-case someone wants to extends our logs
        WEAPON,
        WORLD,

        NULL,
        TABLE,
        STRING,
        INTEGER,
        FLOAT;
    }

    public enum DamageType {
        EXPLOSION,
        SHOT,
        FALL,
        CRUSHED,
        SLASHED,
        BURNED,
        HIT,
        SHOCKED,
        DROWN,
        UNKNOWN,
        POISON,
        MAGIC,
        TRIGGER,
        VEHICLE;
    }

    public static class DamageCause {
        org.bukkit.event.entity.EntityDamageEvent.DamageCause dmg;
        org.bukkit.entity.Entity victim;
        org.bukkit.entity.Entity attacker;

        public DamageCause(EntityDamageEvent.DamageCause dmg, Entity victim, Entity attacker) {
            this.dmg = dmg;
            this.victim = victim;
            this.attacker = attacker;
        }

        public DamageCause(EntityDamageEvent.DamageCause dmg, Entity victim) {
            this.dmg = dmg;
            this.victim = victim;
            this.attacker = null;
        }

        private DamageType GetCause() {
            switch(dmg) {
                case BLOCK_EXPLOSION:
                case ENTITY_EXPLOSION:
                    return DamageType.EXPLOSION;
                case FALL:
                    return DamageType.FALL;
                case LAVA:
                case FIRE:
                case FIRE_TICK:
                case HOT_FLOOR:
                case DRAGON_BREATH:
                    return DamageType.BURNED;
                case POISON:
                    return DamageType.POISON;
                case CRAMMING:
                case SUFFOCATION:
                    return DamageType.CRUSHED;
                case DROWNING:
                    return DamageType.DROWN;
                case LIGHTNING:
                    return DamageType.SHOCKED;
                case PROJECTILE:
                    return DamageType.SHOT;
                case CONTACT:
                    return DamageType.TRIGGER;
                case MAGIC:
                case THORNS:
                case WITHER:
                case VOID:
                    return DamageType.MAGIC;
                case ENTITY_ATTACK:
                    if (attacker != null) {
                        if (attacker instanceof org.bukkit.entity.Player) {
                            Material type = ((org.bukkit.entity.Player) attacker).getInventory().getItemInMainHand().getType();
                            if (type == Material.DIAMOND_SWORD || type == Material.GOLDEN_SWORD || type == Material.IRON_SWORD || type == Material.STONE_SWORD || type == Material.WOODEN_SWORD) {
                                return DamageType.SLASHED;
                            }
                        }
                    }
                    return DamageType.HIT;
                default:
                    return DamageType.UNKNOWN;
            }
        }
    }

    private static class LogDetail {
        DetailType type;
        String key = null;
        Object value;

        public LogDetail(DetailType type, String key, Object value) {
            this.type = type;
            this.key = key;
            this.value = value;
        }

        public LogDetail(DetailType type, Object value) {
            this.type = type;
            this.value = value;
        }
    }

    /**
     * Will create a log event for NetworkManager
     *
     * @param type      The type of the log
     * @param event     The specific event, updated list available in NetworkManager documentation
     */
    public static void Log(LogType type, String event) {
        Log(type, event, null, null);
    }

    /**
     * Will create a log event for NetworkManager
     *
     * @param type      The type of the log
     * @param event     The specific event, updated list available in NetworkManager documentation
     * @param details   A of list of objects, which are then converted into the NetworkManager logDetails
     */
    public static void Log(LogType type, String event, List<Object> details) {
        Log(type, event, null, details);
    }

    /**
     * Will create a log event for NetworkManager
     *
     * @param type      The type of the log
     * @param event     The specific event, updated list available in NetworkManager documentation
     * @param subevent  The specific subevent, updated list available in NetworkManager documentation
     * @param details   A of list of objects, which are then converted into the NetworkManager logDetails
     */
    public static void Log(LogType type, String event, String subevent, List<Object> details) {
        // Perform the details synchronously, in-case the objects may disappear next tick
        List<List<LogDetail>> new_details = new ArrayList<>();
        if (details != null) {
            for (Object obj : details) {
                new_details.add(FormatDetail(obj));
            }
        }

        NetworkManager.getNMPlugin().runAsyncTask(new Runnable() {
            @Override
            public void run() {
                PerformLog(type, event, subevent, new_details);
            }
        });
    }

    public synchronized static void LogSynchronously(LogType type, String event) {
        LogSynchronously(type, event, null, null);
    }

    public synchronized static void LogSynchronously(LogType type, String event, List<Object> details) {
        LogSynchronously(type, event, null, details);
    }

    public synchronized static void LogSynchronously(LogType type, String event, String subevent, List<Object> details) {
        List<List<LogDetail>> new_details = new ArrayList<>();
        if (details != null) {
            for (Object obj : details) {
                new_details.add(FormatDetail(obj));
            }
        }

        PerformLog(type, event, subevent, new_details);
    }

    private static void PerformLog(LogType type, String event, String subevent, List<List<LogDetail>> details) {
        if (NetworkManager.api == null) {
            NetworkManager.LogError("API not loaded for logging!");
            return;
        }
        JsonArray json = new JsonArray();

        for (int i = 0; i < details.size(); i++) {
            for (LogDetail detail : details.get(i)) {
                JsonObject obj = new JsonObject();
                obj.addProperty("type", detail.type.toString());
                obj.addProperty("argid", i);
                if (detail.key != null) {
                    obj.addProperty("vid", detail.key);
                }
                obj.addProperty("value", detail.value.toString());
                json.add(obj);
            }
        }

        Map<String, String> formData = new HashMap<>();
        formData.put("type", type.toString());
        formData.put("event", event);
        formData.put("subevent", subevent);
        formData.put("time", String.valueOf(System.currentTimeMillis() / 1000L));
        formData.put("logDetails", json.toString());

        NetworkManager.api.Put("/server/" + NetworkManager.GetSID() + "/log", formData);
    }

    @SuppressWarnings("rawtypes")
    private static List<LogDetail> FormatDetail(Object obj) {
        List<LogDetail> result = new ArrayList<>();
        if (obj instanceof app.networkmanager.minecraft.types.Player) {
            final app.networkmanager.minecraft.types.Player ply = (app.networkmanager.minecraft.types.Player)obj;
            result.add(new LogDetail(DetailType.NMID, ply.getNMId()));
            result.add(new LogDetail(DetailType.USID, ply.getSessionId()));
        } else if (obj instanceof org.bukkit.entity.Player) {
            final app.networkmanager.minecraft.types.Player ply = NetworkManager.players.get(((org.bukkit.entity.Player)obj).getUniqueId());
            if (ply != null) {
                result.add(new LogDetail(DetailType.NMID, ply.getNMId()));
                result.add(new LogDetail(DetailType.USID, ply.getSessionId()));
            } else {
                result.add(new LogDetail(DetailType.NMID, -1));
                result.add(new LogDetail(DetailType.USID, -1));
            }
            result.add(new LogDetail(DetailType.STRING, "name", ((org.bukkit.entity.Player)obj).getName()));
        } else if (obj instanceof org.bukkit.entity.Entity) {
            // Store some sort of info... at some point... when we learn about entity methods
            result.add(new LogDetail(DetailType.ENTITY, obj.getClass().getSimpleName()));
        } else if (obj instanceof org.bukkit.Material) {
            result.add(new LogDetail(DetailType.ENTITY, ((org.bukkit.Material)obj).name()));
        } else if (obj instanceof org.bukkit.inventory.ItemStack) {
            result.add(new LogDetail(DetailType.WEAPON, ((org.bukkit.inventory.ItemStack)obj).getType().name()));
        } else if (obj instanceof DamageCause) {
            result.add(new LogDetail(DetailType.STRING, ((DamageCause)obj).GetCause().toString()));
            if (((DamageCause)obj).attacker != null && ((DamageCause)obj).attacker instanceof org.bukkit.entity.HumanEntity) {
                result.add(new LogDetail(DetailType.WEAPON, (((org.bukkit.entity.HumanEntity)((DamageCause) obj).attacker).getInventory().getItemInMainHand().getType().name())));
            }
        } else if (obj instanceof String) {
            result.add(new LogDetail(DetailType.STRING, (String)obj));
        } else if (obj instanceof Integer) {
            result.add(new LogDetail(DetailType.INTEGER, (Integer)obj));
        } else if (obj instanceof Long) {
            result.add(new LogDetail(DetailType.INTEGER, (Long)obj));
        } else if (obj instanceof Float) {
            result.add(new LogDetail(DetailType.FLOAT, (Float)obj));
        } else if (obj instanceof Double) {
            result.add(new LogDetail(DetailType.FLOAT, (Double)obj));
        } else if (obj instanceof Map) {
            // Convert into JSON
            JsonObject json = new JsonObject();
            Iterator it = ((Map)obj).entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                json.addProperty(pair.getKey().toString(), pair.getValue().toString());
            }
            //json.putAll((Map)obj);
            result.add(new LogDetail(DetailType.STRING, json.toString()));
        } else if (obj instanceof List) {
            // Convert into JSON
            JsonArray json = new JsonArray();
            for (int i = 0; i < ((List)obj).size(); i++) {
                json.add(((List)obj).get(i).toString());
            }
            result.add(new LogDetail(DetailType.STRING, json.toString()));
        }
        return result;
    }
}
