package app.networkmanager.minecraft;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

abstract class HTTP {
    private static String userAgent = "NetworkManager: Minecraft.Java";
    protected int timeout = 5000;
    protected String endpoint = "";

    public class Response {
        public Integer status;
        public Map<String, List<String>> headers;
        public String content;
        public JsonObject json;

        public Response(Integer status, Map<String, List<String>> headers, String content) {
            this.status = status;
            this.headers = headers;
            this.content = content;
            this.json = null;

            if (headers.containsKey("Content-Type")) {
                if (headers.get("Content-Type").contains("application/json")) {
                    this.json = new JsonParser().parse(this.content).getAsJsonObject();
                }
            }
        }

        public Response(Integer status, Map<String, List<String>> headers, String content, JsonObject json) {
            this.status = status;
            this.headers = headers;
            this.content = content;
            this.json = json;
        }
    }

    protected Response Process(Response resp) {
        return resp;
    }

    protected Map<String, String> Headers() {
        return new HashMap<>();
    }

    private Response SendRequest(String endpoint, String method) {
        return SendRequest(endpoint, method, new HashMap<>());
    }

    private Response SendRequest(String endpoint, String method, Map<String, String> data) {
        return SendRequest(endpoint, method, data, new HashMap<>());
    }

    private Response SendRequest(String endpoint, String method, Map<String, String> data, Map<String, String> addHeaders) {
        return SendRequest(endpoint, method, data, addHeaders, "application/x-www-form-urlencoded");
    }

    private Response SendRequest(String endpoint, String method, Map<String, String> data, Map<String, String> addHeaders, String contentType) {
        NetworkManager.LogHTTP("Send " + method + " request to: " + endpoint);
        String urlParameters = "";
        boolean useAmp = false;
        for (Map.Entry<String, String> entry : data.entrySet()) {
            if (useAmp) {
                urlParameters += "&";
            } else {
                useAmp = true;
            }
            try {
                if (entry.getKey() != null) {
                    urlParameters += URLEncoder.encode(entry.getKey(), "UTF-8");
                }
                urlParameters += "=";
                if (entry.getValue() != null) {
                    urlParameters += URLEncoder.encode(entry.getValue(), "UTF-8");
                }
            } catch (UnsupportedEncodingException e) {
                NetworkManager.sentry.sendException(e);
                // TODO Auto-generated catch block
                //e.printStackTrace();
			}
        }
        byte[] formData = urlParameters.getBytes(StandardCharsets.UTF_8);

        
        Response resp = null;
        URL api = null;
        try {
            api = new URL(endpoint);
            HttpURLConnection conn = (HttpURLConnection) api.openConnection();
            conn.setConnectTimeout(timeout);
            conn.setDoOutput(true);

            conn.setRequestMethod(method);
            conn.setRequestProperty("User-Agent", userAgent);
            for (Map.Entry<String, String> entry : addHeaders.entrySet()) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
            for (Map.Entry<String, String> entry : Headers().entrySet()) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
            if (formData.length > 0) {
                NetworkManager.LogHTTP("Write " + formData.length + " bytes of data: " + urlParameters);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Length", String.valueOf(formData.length));
                conn.setRequestProperty("Content-Type", contentType);

                OutputStream os = conn.getOutputStream();
                os.write(formData);
                os.flush();
                os.close();
            }

            InputStream _is;
            if (conn.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                _is = conn.getInputStream();
            } else {
                /* error from server */
                _is = conn.getErrorStream();
            }
            StringBuilder content;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(_is))) {
                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }

            resp = new Response(conn.getResponseCode(), conn.getHeaderFields(), content.toString());
            conn.disconnect();
        } catch (UnknownHostException e) {
            NetworkManager.sentry.sendException(e);
            NetworkManager.LogError("Unknown host: " + api.getHost());
        } catch (NoRouteToHostException e) {
            NetworkManager.sentry.sendException(e);
            NetworkManager.LogError("Host unreachable: " + api.getHost());
        } catch (MalformedURLException e) {
            NetworkManager.sentry.sendException(e);
            NetworkManager.LogError("Malformed URL: " + api.toExternalForm());
            e.printStackTrace();
        } catch (IOException e) {
            NetworkManager.sentry.sendException(e);
            e.printStackTrace();
        }
        return Process(resp);
    }

    // NetworkManager HTTP implementations
    public Response Any(String action, String method) {
        return Any(action, method, new HashMap<>());
    }

    public Response Any(String action, String method, Map<String, String> data) {
        return Any(action, method, data, new HashMap<>());
    }

    public Response Any(String action, String method, Map<String, String> data, Map<String, String> headers) {
        return SendRequest(endpoint + action, method, data, headers);
    }

    public Response Get(String action) {
        return Get(action, new HashMap<>());
    }

    public Response Get(String action, Map<String, String> headers) {
        return SendRequest(endpoint + action, "GET", new HashMap<>(), headers);
    }

    public Response Post(String action, Map<String, String> data) {
        return Post(action, data, new HashMap<>());
    }

    public Response Post(String action, Map<String, String> data, Map<String, String> headers) {
        return SendRequest(endpoint + action, "POST", data, headers);
    }

    public Response Put(String action) {
        return Put(action, new HashMap<>());
    }

    public Response Put(String action, Map<String, String> data) {
        return Put(action, data, new HashMap<>());
    }

    public Response Put(String action, Map<String, String> data, Map<String, String> headers) {
        return SendRequest(endpoint + action, "PUT", data, headers);
    }

    public Response Delete(String action) {
        return Delete(action, new HashMap<>());
    }

    public Response Delete(String action, Map<String, String> data) {
        return Delete(action, data, new HashMap<>());
    }

    public Response Delete(String action, Map<String, String> data, Map<String, String> headers) {
        return SendRequest(endpoint + action, "DELETE", data, headers);
    }
}