package app.networkmanager.minecraft.listeners;

import app.networkmanager.minecraft.Logs;
import app.networkmanager.minecraft.NetworkManager;
import app.networkmanager.minecraft.events.member.MemberSessionIndexed;
import app.networkmanager.minecraft.events.server.ServerIndexed;
import app.networkmanager.minecraft.events.server.ServerSessionIndexed;
import app.networkmanager.minecraft.types.Player;
import com.google.common.eventbus.Subscribe;

public class NetworkManagerListener implements app.networkmanager.minecraft.events.Listener {
    @Subscribe
    public void onServerIndexed(ServerIndexed e) {
        NetworkManager.LogInfo("Server Index id: " + e.getId());
    }

    @Subscribe
    public void onServerSessionIndexed(ServerSessionIndexed e) {
        Logs.Log(Logs.LogType.SERVER, "start");
    }

    @Subscribe
    public void onMemberLoaded(MemberSessionIndexed e) {
        Player nm_ply = e.getNMPlayer();
        NetworkManager.LogDebug("Received network manager id: " + nm_ply.getNMId() + " session: " + nm_ply.getSessionId() + " Rank: " + nm_ply.getRank().getDisplayName());
        //NetworkManager.LogInfo(e.getNMPlayer().get);
    }
}
